#!/usr/binary/env bash
apt-get -y update
apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    git \
    tar \
    unzip \
    dsniff \
    wfuzz \
    medusa \
    hydra \
    ncat \
    ncrack \
    yersinia \
    wget \
    telnet \
    nmap \
    masscan \
    tmux \
    python-argparse \
    lynx

git clone https://github.com/infoslack/awesome-web-hacking.git
git clone https://github.com/rajkumardusad/onex.git
chmod +x onex/install
sh onex/install
./onex/install

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get -y update
apt-get -y install docker-ce docker-ce-cli containerd.io

wget https://go.dev/dl/go1.17.7.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.7.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/binary
echo "export PATH=$PATH:/usr/local/go/bin" >> /etc/profile

go install github.com/cjbassi/gotop@latest

docker run -d --rm -p 8080:443 --name openvas mikesplain/openvas

wget https://apt.brf.im/cyberghostvpn-ubuntu-20.04-1.3.4.zip
unzip -o cyberghostvpn-ubuntu-20.04-1.3.4.zip
#cd cyberghostvpn-ubuntu-20.04-1.3.4
#bash ./install.sh
#cyberghostvpn --connect --country-code ru
echo "#!/bin/bash" > ./cyberghostvpn.sh
echo "cd cyberghostvpn-ubuntu-20.04-1.3.4 && bash ./install.sh" >> ./cyberghostvpn.sh
echo "cyberghostvpn --connect --country-code ru" >> ./cyberghostvpn.sh

echo '#!/binary/bash' > ./offensive.sh
echo 'docker run --rm -it --name offensive aaaguirrep/offensive-docker $@' >> ./offensive.sh

chmod +x ./cyberghostvpn.sh
chmod +x ./offensive.sh

curl https://check.torproject.org/api/ip
