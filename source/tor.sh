#!/usr/binary/env bash
apt-get -y update
apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    git \
    tar \
    unzip \
    dsniff \
    wfuzz \
    medusa \
    hydra \
    ncat \
    ncrack \
    yersinia \
    wget \
    telnet \
    nmap \
    masscan \
    tmux \
    python-argparse \
    lynx

git clone https://github.com/infoslack/awesome-web-hacking.git
git clone https://github.com/rajkumardusad/onex.git
chmod +x onex/install
sh onex/install
./onex/install

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get -y update
apt-get -y install docker-ce docker-ce-cli containerd.io

wget https://go.dev/dl/go1.17.7.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.7.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/binary
echo "export PATH=$PATH:/usr/local/go/bin" >> /etc/profile

go install github.com/cjbassi/gotop@latest

# TOR
docker run --net=host --name=torproxy_all_traffic --rm -p 8118:8118 -p 9050:9050 -d dperson/torproxy

set -euo pipefail                           # Treat unset variables as an error

# Most of this is from
# https://trac.torproject.org/projects/tor/wiki/doc/TransparentProxy

### set variables
# destinations you don't want routed through Tor
_non_tor="192.168.1.0/24 192.168.0.0/24"

### get the container tor runs in
_tor_container="$(docker ps | awk '/torproxy/ {print $NF; quit}')"
if [[ "$_tor_container" == "" ]]; then
    echo 'ERROR: you must start a tor proxy container first, IE:'
    echo '    docker run -d --net host --restart always dperson/torproxy'
    exit 1
fi

### get the UID that tor runs as
_tor_uid="$(docker exec $_tor_container id -u tor)"

### Tor's transport
_trans_port="9040"
_dns_port="5353"

### flush iptables
iptables -F
iptables -t nat -F

### set iptables *nat to ignore tor user
iptables -t nat -A OUTPUT -m owner --uid-owner $_tor_uid -j RETURN

### redirect all DNS output to tor's DNSPort
iptables -t nat -A OUTPUT -p udp --dport 53 -j REDIRECT --to-ports $_dns_port

### set iptables *filter
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

### allow clearnet access for hosts in $_non_tor
for _clearnet in $_non_tor 127.0.0.0/8; do
   iptables -t nat -A OUTPUT -d $_clearnet -j RETURN
   iptables -A OUTPUT -d $_clearnet -j ACCEPT
done

### redirect all other output to tor's transport
iptables -t nat -A OUTPUT -p tcp --syn -j REDIRECT --to-ports $_trans_port

### allow only tor output
iptables -A OUTPUT -m owner --uid-owner $_tor_uid -j ACCEPT
iptables -A OUTPUT -j REJECT

curl https://check.torproject.org/api/ip
