VERSION?=$(shell git describe --tags)

format:
	gofmt -w=true **/*.go

build:
	CGO_ENABLED=0 env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o bin/apt-server ./cmd/server

build-territorialdefence:
	CGO_ENABLED=0 env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o bin/territorialdefence ./cmd/territorialdefence

build-refugees:
	CGO_ENABLED=0 env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o bin/refugees ./cmd/refugees

build-echo:
	CGO_ENABLED=0 env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o bin/echo-server ./cmd/echo

test:
	go test -timeout 60s -race -p 1 ./...

test-cover:
	go test -cover -p 1 ./...

init:
	chmod -R +x .githooks/
	mkdir -p .git/hooks/
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

lint:
	golangci-lint run --exclude-use-default=false --skip-dirs=vendor --disable-all --enable=goimports --enable=gosimple --enable=typecheck --enable=unused --enable=golint --enable=deadcode --enable=structcheck --enable=varcheck --enable=errcheck --enable=ineffassign --enable=govet --enable=staticcheck  --deadline=3m ./...

.PHONY: build test test-cover format init lint