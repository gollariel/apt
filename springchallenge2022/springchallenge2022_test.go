package springchallenge2022

import (
	"fmt"
	"math"
	"sort"
	"testing"
)

type Base struct {
	Health   int
	Mana     int
	Position Point
}
type Point struct {
	X, Y int
}

// DistanceSquare return distance square between positions (euclidean)
func (p Point) DistanceSquare(p2 Point) int {
	d1 := p.X - p2.X
	d2 := p.Y - p2.Y
	sum := (d1 * d1) + (d2 + d2)
	return sum
}

// Distance return distance between positions (euclidean)
func (p Point) Distance(p2 Point) float64 {
	return math.Sqrt(float64(p.DistanceSquare(p2)))
}

func (p Point) Add(p2 Point) Point {
	return Point{
		X: p.X + p2.X,
		Y: p.Y + p2.Y,
	}
}

func (p Point) Scale(val int) Point {
	return Point{
		X: p.X * val,
		Y: p.Y * val,
	}
}

// id: Unique identifier
// _type: 0=monster, 1=your hero, 2=opponent hero
// x: Position of this entity
// shieldLife: Ignore for this league; Count down until shield spell fades
// isControlled: Ignore for this league; Equals 1 when this entity is under a control spell
// health: Remaining health of this monster
// vx: Trajectory of this monster
// nearBase: 0=monster with no target yet, 1=monster targeting a base
// threatFor: Given this monster's trajectory, is it a threat to 1=your base, 2=your opponent's base, 0=neither

type Entity struct {
	Position     Point
	SpeedVector  Point
	ID           int
	ShieldLife   int
	IsControlled int
	Health       int
	NearBase     int
	ThreatFor    int
}

type Hero struct {
	*Entity
	Next Point
}

type Enemy struct {
	*Entity
}

type Monster struct {
	*Entity
}

func ScanEntites() ([]*Monster, []*Hero, []*Enemy) {
	var monsters []*Monster
	var heroes []*Hero
	var enemies []*Enemy
	var entityCount int

	fmt.Scan(&entityCount)
	for i := 0; i < entityCount; i++ {
		e := &Entity{}
		var entityType int
		fmt.Scan(&e.ID, &entityType, &e.Position.X, &e.Position.Y, &e.ShieldLife, &e.IsControlled, &e.Health, &e.SpeedVector.X, &e.SpeedVector.Y, &e.NearBase, &e.ThreatFor)
		switch entityType {
		case 0:
			monsters = append(monsters, &Monster{
				Entity: e,
			})
		case 1:
			heroes = append(heroes, &Hero{
				Entity: e,
			})
		case 2:
			enemies = append(enemies, &Enemy{
				Entity: e,
			})
		}
	}

	return monsters, heroes, enemies
}

func ChooseHeroesActions(base *Base, monsters []*Monster, heroes []*Hero, enemies []*Enemy) []string {
	sort.Slice(monsters, func(i, j int) bool {
		cd1 := monsters[i].Position.DistanceSquare(base.Position)
		fd1 := monsters[i].Position.Add(monsters[i].SpeedVector).DistanceSquare(base.Position)
		if cd1 < fd1 {
			return false
		}
		cd2 := monsters[j].Position.DistanceSquare(base.Position)
		return cd1 < cd2
	})
	var result []string
	for _, monster := range monsters {
		sort.Slice(heroes, func(i, j int) bool {
			return heroes[i].Position.DistanceSquare(monster.Position) < heroes[j].Position.DistanceSquare(monster.Position)
		})
		hero := heroes[0]

		d := hero.Position.Distance(monster.Position)
		var vector Point
		if d > 1 {
			vector = monster.SpeedVector.Scale(int(d))
		} else {
			vector = monster.SpeedVector
		}
		hero.Next = monster.Position.Add(monster.Position.Add(vector))

		// In the first league: MOVE <x> <y> | WAIT; In later leagues: | SPELL <spellParams>;
		result = append(result, fmt.Sprintf("MOVE %d %d", hero.Next.X, hero.Next.Y))

		if len(heroes) == 1 {
			break
		}
		heroes = heroes[1:]
	}

	if len(result) < 3 {
		for i := len(result); i < 3; i++ {
			result = append(result, "WAIT")
		}
	}
	return result
}

func TestSpringChallenge2022(t *testing.T) {

}
