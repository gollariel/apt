package shapes

import (
	"math"
	"sync"

	"bitbucket.org/espinpro/go-engine/utils/mathutils"
)

// ModifiablePoint special type for reduce allocations
type ModifiablePoint struct {
	coordinates [3]float64
	mu          sync.RWMutex
}

// NewModifiablePoint return new modifiable point
func NewModifiablePoint(s ...float64) *ModifiablePoint {
	var x, y, z float64
	l := len(s)
	switch l {
	case 3: // nolint:gomnd
		x, y, z = s[0], s[1], s[2]
	case 2: // nolint:gomnd
		x, y = s[0], s[1]
	case 1: // nolint:gomnd
		x = s[0]
	}

	return &ModifiablePoint{
		coordinates: [3]float64{x, y, z},
	}
}

// Coordinate return coordinate for dimension (0-x, 1-y, 2-z)
func (p *ModifiablePoint) Coordinate(i int) (r float64) {
	p.mu.RLock()
	if i <= 2 && i >= 0 {
		r = p.coordinates[i]
	}
	p.mu.RUnlock()
	return
}

// SetCoordinate set coordinate for dimension (0-x, 1-y, 2-z)
func (p *ModifiablePoint) SetCoordinate(i int, v float64) *ModifiablePoint {
	p.mu.Lock()
	p.coordinates[i] = v
	p.mu.Unlock()
	return p
}

// SetCoordinates set coordinates for dimension (0-x, 1-y, 2-z)
func (p *ModifiablePoint) SetCoordinates(v [3]float64) *ModifiablePoint {
	p.mu.Lock()
	p.coordinates = v
	p.mu.Unlock()
	return p
}

// Coordinates return all coordinates
func (p *ModifiablePoint) Coordinates() [3]float64 {
	p.mu.RLock()
	c := p.coordinates
	p.mu.RUnlock()
	return c
}

// DistanceSquare return distance square between positions (euclidean)
func (p *ModifiablePoint) DistanceSquare(p2 Vector) float64 {
	var sum float64
	for i := range p.Coordinates() {
		d := p.Coordinate(i) - p2.Coordinate(i)
		sum += d * d
	}
	return sum
}

// Distance return distance between positions (euclidean)
func (p *ModifiablePoint) Distance(p2 Vector) float64 {
	return math.Sqrt(p.DistanceSquare(p2))
}

// Dot scalar multiply
func (p *ModifiablePoint) Dot(p2 Vector) float64 {
	var sum float64
	for i, v := range p.Coordinates() {
		sum += v * p2.Coordinate(i)
	}
	return sum
}

// Normal returns the vector's norm.
func (p *ModifiablePoint) Normal() float64 {
	return math.Sqrt(p.Dot(p))
}

// NormalSquare returns the vector's norm square.
func (p *ModifiablePoint) NormalSquare() float64 {
	return p.Dot(p)
}

// Scale multiply point on given point
func (p *ModifiablePoint) Scale(v float64) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i * v
	}
	p.mu.Unlock()
	return p
}

// Increase add value to given point
func (p *ModifiablePoint) Increase(v float64) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i + v
	}
	p.mu.Unlock()
	return p
}

// Decrease remove value from given point
func (p *ModifiablePoint) Decrease(v float64) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i - v
	}
	p.mu.Unlock()
	return p
}

// Normalize normalize point
func (p *ModifiablePoint) Normalize() *ModifiablePoint {
	n2 := math.Abs(p.Normal())
	if mathutils.ApproximatelyEqual(n2, 0) {
		return p.Scale(0)
	}
	p.Scale(1 / n2)
	return p
}

// Add add diff to p
func (p *ModifiablePoint) Add(diff Vector) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i + diff.Coordinate(k)
	}
	p.mu.Unlock()
	return p
}

// Round round all coordinate with precision
func (p *ModifiablePoint) Round(precision float64) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = mathutils.RoundWithPrecision(i, precision)
	}
	p.mu.Unlock()
	return p
}

// Subtract get diff between points
func (p *ModifiablePoint) Subtract(diff Vector) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i - diff.Coordinate(k)
	}
	p.mu.Unlock()
	return p
}

// Multiply get multiple point
func (p *ModifiablePoint) Multiply(diff Vector) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i * diff.Coordinate(k)
	}
	p.mu.Unlock()
	return p
}

// Divide get divide point
func (p *ModifiablePoint) Divide(diff Vector) *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		c := diff.Coordinate(k)
		if c != 0 {
			p.coordinates[k] = i / c
		}
	}
	p.mu.Unlock()
	return p
}

// Copy return copy point
func (p *ModifiablePoint) Copy() *ModifiablePoint {
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range p.Coordinates() {
		np[k] = v
	}
	return NewModifiablePoint(np...)
}

// Invert invert the point
func (p *ModifiablePoint) Invert() *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = i * -1 // nolint:gomnd
	}
	p.mu.Unlock()
	return p
}

// Abs abs the point
func (p *ModifiablePoint) Abs() *ModifiablePoint {
	p.mu.Lock()
	for k, i := range p.coordinates {
		p.coordinates[k] = math.Abs(i)
	}
	p.mu.Unlock()
	return p
}

// ToPoint return classic not modifiable point
func (p *ModifiablePoint) ToPoint() Point {
	np := make([]float64, 3) // nolint:gomnd
	for k, i := range p.Coordinates() {
		np[k] = i
	}
	return NewPoint(np...)
}

// Cross calculate cross
func (p *ModifiablePoint) Cross(p2 Vector) *ModifiablePoint {
	x := p.Coordinate(1)*p2.Coordinate(2) - p.Coordinate(2)*p2.Coordinate(1) // nolint:gomnd
	y := p.Coordinate(2)*p2.Coordinate(0) - p.Coordinate(0)*p2.Coordinate(2) // nolint:gomnd
	z := p.Coordinate(0)*p2.Coordinate(1) - p.Coordinate(1)*p2.Coordinate(0) // nolint:gomnd
	return p.SetCoordinates([3]float64{x, y, z})
}

// Reflect reflection by normal
func (p *ModifiablePoint) Reflect(normalPoint *ModifiablePoint) *ModifiablePoint {
	normal := normalPoint.Copy()
	return p.Subtract(normal.Scale(2 * p.Dot(normal))) // nolint:gomnd
}

// Refract refraction by normal and eta
func (p *ModifiablePoint) Refract(normalPoint *ModifiablePoint, eta float64) *ModifiablePoint {
	normal := normalPoint.Copy()
	n := normal.Dot(p)
	k := 1 - eta*eta*(1-n*n)
	if k < 0 {
		return p
	}
	return p.Scale(eta).Subtract(normal.Scale(eta*n + math.Sqrt(k)))
}
