package shapes

import (
	"math"

	"bitbucket.org/espinpro/go-engine/utils/mathutils"
)

// Quaternion represents a quaternion. We use the notation : q = (x*i, y*j, z*k, w) to represent a quaternion.
type Quaternion struct {
	Point
	w float64
}

// NewQuaternion return new qsuaternion
func NewQuaternion(p Point, w float64) Quaternion {
	return Quaternion{
		Point: p,
		w:     w,
	}
}

// Point1 return point
func (q Quaternion) Point1() Point {
	return q.Point
}

// GetW return 4th component
func (q Quaternion) GetW() float64 {
	return q.w
}

// Dot scalar product between two quaternions
func (q Quaternion) Dot(q2 Quaternion) float64 {
	var sum float64
	for i, v := range q.Point1().Coordinates() {
		sum += v * q2.Point1().Coordinate(i)
	}
	sum += q.GetW() * q2.GetW()
	return sum
}

// Normal return the length of the quaternion (public )
func (q Quaternion) Normal() float64 {
	return math.Sqrt(q.NormalSquare())
}

// NormalSquare the square of the  normal of the quaternion
func (q Quaternion) NormalSquare() float64 {
	return q.Dot(q)
}

// Add addition with assignment
func (q Quaternion) Add(q2 Quaternion) Quaternion {
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range q.Point1().Coordinates() {
		np[k] = v + q2.Coordinate(k)
	}
	return NewQuaternion(NewPoint(np...), q.GetW()+q2.GetW())
}

// Conjugate return the conjugate of the quaternion (public )
func (q Quaternion) Conjugate() Quaternion {
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range q.Point1().Coordinates() {
		np[k] = -v
	}
	return NewQuaternion(NewPoint(np...), -q.GetW())
}

// Inverse inverse the quaternion
func (q Quaternion) Inverse() Quaternion {
	lengthSquare := q.NormalSquare()
	if lengthSquare <= mathutils.MachineEpsilon {
		return q
	}

	np := make([]float64, 3) // nolint:gomnd
	for k := range q.coordinates {
		np[k] /= -lengthSquare
	}
	return NewQuaternion(NewPoint(np...), q.GetW()/lengthSquare)
}

// Scale with a constant
func (q Quaternion) Scale(number float64) Quaternion {
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range q.Point1().Coordinates() {
		np[k] = v * number
	}
	return NewQuaternion(NewPoint(np...), q.GetW()*number)
}

// Multiply multiplication of two quaternions
func (q Quaternion) Multiply(q2 Quaternion) Quaternion {
	q1V := q.Point1()
	q2V := q2.Point1()

	newVector := q2V.Scale(q.GetW()).Add(q1V.Scale(q2.GetW())).Add(q1V.Cross(q2V))

	return NewQuaternion(newVector, q.GetW()*q2.GetW()-q1V.Dot(q2V))
}

// Normalize normalize the quaternion
func (q Quaternion) Normalize() Quaternion {
	l := q.Normal()
	if l <= mathutils.MachineEpsilon {
		return q
	}
	li := 1 / l
	return q.Scale(li)
}

// Subtract subtraction with assignment
func (q Quaternion) Subtract(q2 Quaternion) Quaternion {
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range q.Point1().Coordinates() {
		np[k] = v - q2.Coordinate(k)
	}
	return NewQuaternion(NewPoint(np...), q.GetW()-q2.GetW())
}

// MultiplyWithVector the multiplication with a vector.
// This methods rotates a point given the rotation of a quaternion.
func (q Quaternion) MultiplyWithVector(vector Point) Point {
	c := q.Conjugate()
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range vector.Coordinates() {
		np[k] = v
	}
	p := NewQuaternion(NewPoint(np...), 0)
	vectorOut := q.Multiply(p).Multiply(c).Point1()
	return vectorOut
}

// Copy copy quaternion
func (q Quaternion) Copy() Quaternion {
	np := make([]float64, 3) // nolint:gomnd
	for k, v := range q.Point1().Coordinates() {
		np[k] = v
	}
	return NewQuaternion(NewPoint(np...), q.GetW())
}

// Slerp compute the spherical linear interpolation between two quaternions.
// The t argument has to be such that 0 <= t <= 1. This method is static.
func (q Quaternion) Slerp(oldQuaternion Quaternion, newQuaternion2 Quaternion, t float64) (Quaternion, error) {
	if t < 0 || t > 1 { // nolint:gomnd
		return q, ErrUnexpectedValue
	}

	var invert float64 = 1 // nolint:gomnd
	tempQ2 := newQuaternion2.Copy()

	// Compute cos(theta) using the quaternion scalar product
	cosineTheta := oldQuaternion.Dot(newQuaternion2)
	if cosineTheta < 0 {
		cosineTheta = -cosineTheta
		invert = -1 // nolint:gomnd
	}

	// Because of precision, if cos(theta) is nearly 1,
	// therefore theta is nearly 0 and we can write
	// sin((1-t)*theta) as (1-t) and sin(t*theta) as t
	epsilon := 0.00001 // nolint:gomnd
	if 1-cosineTheta < epsilon {
		quaternionOut := oldQuaternion.Scale(1 - t).Add(tempQ2.Scale(t * invert))
		return quaternionOut, nil
	}

	// Compute the theta angle
	theta := math.Acos(cosineTheta)
	// Compute sin(theta)
	sinTheta := math.Sin(theta)
	coeff1 := math.Sin((1-t)*theta) / sinTheta
	coeff2 := math.Sin(t*theta) / sinTheta * invert
	return oldQuaternion.Scale(coeff1).Add(tempQ2.Scale(coeff2)), nil
}

// Equals return true, if quaternion the same
func (q Quaternion) Equals(obj Spatial) bool {
	if obj == nil {
		return false
	}

	other, ok := obj.(Quaternion)
	if !ok {
		return false
	}

	if !q.Point1().Equals(other.Point1()) {
		return false
	}

	return mathutils.ApproximatelyEqual(q.GetW(), other.GetW())
}
