package shapes

import (
	"math"

	"bitbucket.org/espinpro/go-engine/utils/mathutils"
)

const (
	angle180 float64 = 180
	angle360 float64 = 360
)

// OrbitalPosition contains Angle and Radius
type OrbitalPosition struct {
	Radius int
	Angle  float64
}

// NewOrbitalPosition return new orbital position
func NewOrbitalPosition(radius int, angle float64) *OrbitalPosition {
	return &OrbitalPosition{
		Radius: radius,
		Angle:  angle,
	}
}

// AddAngle add angle to orbital position
func (o *OrbitalPosition) AddAngle(angle float64) {
	c := float64(int(angle / angle360))
	angle -= c * angle360
	o.Angle += angle
	if o.Angle >= angle360 {
		o.Angle -= angle360
	}
}

// GetAngleWithPrecision return angle with given precision
func (o *OrbitalPosition) GetAngleWithPrecision(precision float64) float64 {
	return mathutils.RoundWithPrecision(o.Angle, precision)
}

// CalculateRemainingTicks return calculated remaining ticks
func CalculateRemainingTicks(from, to *OrbitalPosition, speed int) uint32 {
	var ticks uint32
	var inGoal bool
	for {
		ticks++
		from, inGoal = TravelBetweenOrbitalPositions(from, to, speed)
		if inGoal {
			break
		}
	}
	return ticks
}

// TravelBetweenOrbitalPositions calculate new position for ship group
func TravelBetweenOrbitalPositions(from, to *OrbitalPosition, speed int) (*OrbitalPosition, bool) {
	var inGoalRadius bool
	var inGoalAngle bool

	var r int
	if from.Radius-speed > to.Radius { //nolint:gocritic
		r = from.Radius - speed
	} else if from.Radius+speed < to.Radius {
		r = from.Radius + speed
	} else {
		r = to.Radius
		inGoalRadius = true
	}

	if speed > from.Radius+r {
		speed = from.Radius + r
	}

	diff := RadianToDegree(FoundAngle(float64(from.Radius), float64(r), float64(speed)))
	if to.Angle-from.Angle > angle180 {
		from.Angle = angle360
	}

	var a float64
	if from.Angle-diff > to.Angle { //nolint:gocritic
		a = from.Angle - diff
	} else if from.Angle+diff < to.Angle {
		a = from.Angle + diff
	} else {
		a = to.Angle
		inGoalAngle = true
	}

	return &OrbitalPosition{
		Radius: r,
		Angle:  a,
	}, inGoalRadius && inGoalAngle
}

// FoundAngle return angle by The Law of Cosines
func FoundAngle(radius, radius2, speed float64) float64 {
	return math.Acos((math.Pow(radius, 2) + math.Pow(radius2, 2) - math.Pow(speed, 2)) / (2 * radius * radius2)) //nolint:gomnd
}
