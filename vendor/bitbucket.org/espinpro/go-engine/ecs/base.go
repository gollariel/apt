package ecs

import (
	"sync/atomic"

	"bitbucket.org/espinpro/go-engine/registry"
)

// RegistryName name of specific registry instance
const RegistryName = "ecs"

// BaseEntity contains required fields
type BaseEntity struct {
	id      uint64
	version uint64
}

// NewBaseEntity return new entity
func NewBaseEntity() *BaseEntity {
	id := registry.Get(RegistryName).NextID()
	return &BaseEntity{
		id:      id,
		version: 1,
	}
}

// NewBaseEntityWithID return new entity with id
func NewBaseEntityWithID(id uint64) *BaseEntity {
	return &BaseEntity{
		id:      id,
		version: 1,
	}
}

// GetID return id
func (e *BaseEntity) GetID() uint64 {
	return e.id
}

// SetID set id
func (e *BaseEntity) SetID(id uint64) {
	lid := registry.Get(RegistryName).LatestID()
	if id > lid {
		registry.Get(RegistryName).SetLatestID(id)
	}
	e.id = id
}

// GetVersion return current version of object
func (e *BaseEntity) GetVersion() uint64 {
	return atomic.LoadUint64(&e.version)
}

// UpVersion increase current version
func (e *BaseEntity) UpVersion() {
	atomic.AddUint64(&e.version, 1)
}

// SetVersion set current version
func (e *BaseEntity) SetVersion(v uint64) {
	atomic.StoreUint64(&e.version, v)
}
