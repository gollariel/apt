package datatypes

import (
	"strings"
)

// DataSet is a set of interfaces
// It can test for in O(1) time
type DataSet map[interface{}]struct{}

// NewDataSet returns a new DataSet from the given data
func NewDataSet(input ...interface{}) DataSet {
	set := DataSet{}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set DataSet) Add(key interface{}) {
	set[key] = struct{}{}
}

// Delete delete value
func (set DataSet) Delete(key interface{}) {
	delete(set, key)
}

// Contains tests the membership of given key in the set
func (set DataSet) Contains(key interface{}) (exists bool) {
	_, exists = set[key]
	return
}

// ToSlice return slice of entities
func (set DataSet) ToSlice() []interface{} {
	s := make([]interface{}, len(set))
	var i int
	for k := range set {
		s[i] = k
		i++
	}
	return s
}

// OrderedDataSetStrings is a set of strings
// It can test for in O(1) time
type OrderedDataSetStrings struct {
	data  map[string]struct{}
	order []string
}

// NewOrderedDataSetStrings returns a new OrderedDataSetStrings from the given data
func NewOrderedDataSetStrings(input ...string) OrderedDataSetStrings {
	set := OrderedDataSetStrings{
		data: map[string]struct{}{},
	}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set *OrderedDataSetStrings) Add(key string) {
	if _, ok := set.data[key]; !ok {
		set.data[key] = struct{}{}
		set.order = append(set.order, key)
	}
}

// Delete delete value
func (set *OrderedDataSetStrings) Delete(key string) {
	if _, ok := set.data[key]; ok {
		for i, key2 := range set.order {
			if strings.EqualFold(key, key2) {
				set.order = append(set.order[:i], set.order[i+1:]...)
			}
		}
		delete(set.data, key)
	}
}

// Contains tests the membership of given key in the set
func (set *OrderedDataSetStrings) Contains(key string) (exists bool) {
	_, exists = set.data[key]
	return
}

// ToSlice return slice of entities
func (set *OrderedDataSetStrings) ToSlice() []string {
	s := make([]string, len(set.order))
	copy(s, set.order)
	return s
}

// Count return count of elements
func (set *OrderedDataSetStrings) Count() int {
	return len(set.order)
}

// DataSetStrings is a set of strings
// It can test for in O(1) time
type DataSetStrings map[string]struct{}

// NewDataSetStrings returns a new DataSetStrings from the given data
func NewDataSetStrings(input ...string) DataSetStrings {
	set := DataSetStrings{}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set DataSetStrings) Add(key string) {
	set[key] = struct{}{}
}

// Delete delete value
func (set DataSetStrings) Delete(key string) {
	delete(set, key)
}

// Contains tests the membership of given key in the set
func (set DataSetStrings) Contains(key string) (exists bool) {
	_, exists = set[key]
	return
}

// ToSlice return slice of entities
func (set DataSetStrings) ToSlice() []string {
	s := make([]string, len(set))
	var i int
	for k := range set {
		s[i] = k
		i++
	}
	return s
}

// DataSetInts is a set of integers
// It can test for in O(1) time
type DataSetInts map[int]struct{}

// NewDataSetInts returns a new DataSetInt from the given data
func NewDataSetInts(input ...int) DataSetInts {
	set := DataSetInts{}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set DataSetInts) Add(key int) {
	set[key] = struct{}{}
}

// Delete delete value
func (set DataSetInts) Delete(key int) {
	delete(set, key)
}

// Contains tests the membership of given key in the set
func (set DataSetInts) Contains(key int) (exists bool) {
	_, exists = set[key]
	return
}

// ToSlice return slice of entities
func (set DataSetInts) ToSlice() []int {
	s := make([]int, len(set))
	var i int
	for k := range set {
		s[i] = k
		i++
	}
	return s
}

// MultiUint32Set set for check uint32
type MultiUint32Set map[uint32]map[uint32]struct{}

// NewMultiUint32Set return new set
func NewMultiUint32Set() MultiUint32Set {
	return MultiUint32Set{}
}

// Add add new item
func (s MultiUint32Set) Add(id, id2 uint32) {
	if id > id2 {
		id, id2 = id2, id
	}
	if _, ok := s[id]; !ok {
		s[id] = map[uint32]struct{}{}
	}
	s[id][id2] = struct{}{}
}

// Contains check item
func (s MultiUint32Set) Contains(id, id2 uint32) bool {
	if id > id2 {
		id, id2 = id2, id
	}
	_, exists := s[id][id2]
	return exists
}

// Delete remove items
func (s MultiUint32Set) Delete(id, id2 uint32) {
	if id > id2 {
		id, id2 = id2, id
	}
	delete(s[id], id2)
}
