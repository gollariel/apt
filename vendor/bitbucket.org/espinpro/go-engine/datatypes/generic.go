package datatypes

// Number any nu,bers interface
type Number interface {
	int64 | int32 | int | int16 | int8 | uint64 | uint32 | uint | uint16 | uint8 | float64 | float32
}

// GenericDataSet data set support any string or number as key
// It can test for in O(1) time
type GenericDataSet[K string | Number] map[K]struct{}

// NewGenericDataSet returns a new GenericDataSet from the given data
func NewGenericDataSet[K string | Number](input ...K) GenericDataSet[K] {
	set := GenericDataSet[K]{}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set GenericDataSet[K]) Add(key K) {
	set[key] = struct{}{}
}

// Delete delete value
func (set GenericDataSet[K]) Delete(key K) {
	delete(set, key)
}

// Contains tests the membership of given key in the set
func (set GenericDataSet[K]) Contains(key K) (exists bool) {
	_, exists = set[key]
	return
}

// Count return count of elements
func (set GenericDataSet[K]) Count() int {
	var i int
	for range set {
		i++
	}
	return i
}

// ToSlice return slice of entities
func (set GenericDataSet[K]) ToSlice() []K {
	s := make([]K, set.Count())
	var i int
	for k := range set {
		s[i] = k
		i++
	}
	return s
}

// GenericOrderedDataSet is a set of strings or numbers
// It can test for in O(1) time
type GenericOrderedDataSet[K string | Number] struct {
	data  map[K]struct{}
	order []K
}

// NewGenericOrderedDataSet returns a new GenericOrderedDataSet from the given data
func NewGenericOrderedDataSet[K string | Number](input ...K) GenericOrderedDataSet[K] {
	set := GenericOrderedDataSet[K]{
		data: map[K]struct{}{},
	}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set *GenericOrderedDataSet[K]) Add(key K) {
	if _, ok := set.data[key]; !ok {
		set.data[key] = struct{}{}
		set.order = append(set.order, key)
	}
}

// Delete delete value
func (set *GenericOrderedDataSet[K]) Delete(key K) {
	if _, ok := set.data[key]; ok {
		for i, key2 := range set.order {
			if key == key2 {
				set.order = append(set.order[:i], set.order[i+1:]...)
			}
		}
		delete(set.data, key)
	}
}

// Contains tests the membership of given key in the set
func (set *GenericOrderedDataSet[K]) Contains(key K) (exists bool) {
	_, exists = set.data[key]
	return
}

// ToSlice return slice of entities
func (set *GenericOrderedDataSet[K]) ToSlice() []K {
	s := make([]K, set.Count())
	copy(s, set.order)
	return s
}

// Count return count of elements
func (set *GenericOrderedDataSet[K]) Count() int {
	var i int
	for range set.order {
		i++
	}
	return i
}
