package sortedset

import (
	"encoding/json"
	"math/rand"
	"sync"

	"bitbucket.org/espinpro/go-engine/registry/comparator"
)

// pre-defined values
const (
	skiplistMaxlevel = 32
	skiplistP        = 0.25
)

// SortedSet describe sorted set
type SortedSet struct {
	header     *Node
	tail       *Node
	length     uint64
	level      int
	dict       map[interface{}]*Node
	mu         sync.RWMutex
	comparator comparator.Comparator
	emptyKey   interface{}
}

// CreateNode create new node
func createNode(level int, key interface{}, value interface{}) *Node {
	node := Node{
		key:   key,
		value: value,
		level: make([]Level, level),
	}
	return &node
}

// RandomLevel returns a random level for the new skiplist node we are going to create.
// The return value of this function is between 1 and skiplistMaxlevel
// (both inclusive), with a powerlaw-alike distribution where higher
// levels are less likely to be returned.
func randomLevel() int {
	level := 1
	for float64(rand.Int31()&0xFFFF) < float64(skiplistP*0xFFFF) { //nolint:gosec,gomnd
		level++
	}
	if level < skiplistMaxlevel {
		return level
	}

	return skiplistMaxlevel
}

// InsertNode insert new node
func (s *SortedSet) insertNode(key interface{}, value interface{}) *Node {
	var update [skiplistMaxlevel]*Node
	var rank [skiplistMaxlevel]uint64

	x := s.header
	for i := s.level - 1; i >= 0; i-- {
		/* store rank that is crossed to reach the insert position */
		if s.level-1 == i {
			rank[i] = 0
		} else {
			rank[i] = rank[i+1]
		}

		for x.level[i].forward != nil &&
			(s.comparator(x.level[i].forward.key, key) < 0 ||
				(s.comparator(x.level[i].forward.key, key) == 0 && // key is the same but the key is different
					x.level[i].forward.value != value)) {
			rank[i] += x.level[i].span
			x = x.level[i].forward
		}
		update[i] = x
	}

	/* we assume the key is not already inside, since we allow duplicated
	 * keys, and the re-insertion of key and redis object should never
	 * happen since the caller of Insert() should test in the hash table
	 * if the element is already inside or not. */
	level := randomLevel()

	if level > s.level { // add a new level
		for i := s.level; i < level; i++ {
			rank[i] = 0
			update[i] = s.header
			update[i].level[i].span = s.length
		}
		s.level = level
	}

	x = createNode(level, key, value)
	for i := 0; i < level; i++ {
		x.level[i].forward = update[i].level[i].forward
		update[i].level[i].forward = x

		/* update span covered by update[i] as x is inserted here */
		x.level[i].span = update[i].level[i].span - (rank[0] - rank[i])
		update[i].level[i].span = (rank[0] - rank[i]) + 1
	}

	/* increment span for untouched levels */
	for i := level; i < s.level; i++ {
		update[i].level[i].span++
	}

	if update[0] == s.header {
		x.backward = nil
	} else {
		x.backward = update[0]
	}
	if x.level[0].forward != nil {
		x.level[0].forward.backward = x
	} else {
		s.tail = x
	}
	s.length++
	return x
}

/* DeleteNode internal function used by delete, DeleteBykey and DeleteByRank */
func (s *SortedSet) deleteNode(x *Node, update [skiplistMaxlevel]*Node) {
	for i := 0; i < s.level; i++ {
		if update[i].level[i].forward == x {
			update[i].level[i].span += x.level[i].span - 1
			update[i].level[i].forward = x.level[i].forward
		} else {
			update[i].level[i].span--
		}
	}
	if x.level[0].forward != nil {
		x.level[0].forward.backward = x.backward
	} else {
		s.tail = x.backward
	}
	for s.level > 1 && s.header.level[s.level-1].forward == nil {
		s.level--
	}
	s.length--
	delete(s.dict, x.value)
}

/* Delete delete an element with matching key/key from the skiplist. */
func (s *SortedSet) delete(key interface{}, value interface{}) bool {
	var update [skiplistMaxlevel]*Node

	x := s.header
	for i := s.level - 1; i >= 0; i-- {
		for x.level[i].forward != nil && s.comparator(x.level[i].forward.key, key) < 0 {
			x = x.level[i].forward
		}
		update[i] = x
	}
	/* We may have multiple elements with the same key, what we need
	 * is to find the element with both the right key and object. */
	x = x.level[0].forward
	if x != nil && key == x.key && x.value == value {
		s.deleteNode(x, update)
		// free x
		return true
	}

	return false /* not found */
}

// NewSortedSet create a new SortedSet
func NewSortedSet(emptyKey interface{}, c comparator.Comparator) *SortedSet {
	sortedSet := SortedSet{
		level:      1,
		dict:       make(map[interface{}]*Node),
		comparator: c,
		emptyKey:   emptyKey,
	}
	sortedSet.header = createNode(skiplistMaxlevel, emptyKey, nil)
	return &sortedSet
}

// GetCount return the number of elements
func (s *SortedSet) GetCount() int {
	s.mu.RLock()
	l := int(s.length)
	s.mu.RUnlock()
	return l
}

// PeekMin get the element with minimum key, nil if the set is empty
// Time complexity of this method is : O(log(N))
func (s *SortedSet) PeekMin() *Node {
	s.mu.RLock()
	f := s.header.level[0].forward
	s.mu.RUnlock()
	return f
}

// PopMin get and remove the element with minimal key, nil if the set is empty
// Time complexity of this method is : O(log(N))
func (s *SortedSet) PopMin() *Node {
	s.mu.Lock()
	x := s.header.level[0].forward
	if x != nil {
		s.Remove(x.value)
	}
	s.mu.Unlock()
	return x
}

// PeekMax get the element with maximum key, nil if the set is empty
// Time Complexity : O(1)
func (s *SortedSet) PeekMax() *Node {
	s.mu.RLock()
	t := s.tail
	s.mu.RUnlock()
	return t
}

// PopMax get and remove the element with maximum key, nil if the set is empty
// Time complexity of this method is : O(log(N))
func (s *SortedSet) PopMax() *Node {
	s.mu.Lock()
	x := s.tail
	if x != nil {
		s.Remove(x.value)
	}
	s.mu.Unlock()
	return x
}

// Upsert add an element into the sorted set with specific key / value / key.
// if the element is added, this method returns true; otherwise false means updated
// Time complexity of this method is : O(log(N))
func (s *SortedSet) Upsert(key interface{}, value interface{}) bool {
	var newNode *Node
	s.mu.Lock()

	found := s.dict[value]
	if found != nil {
		// key does not change, only update value
		if s.comparator(found.key, key) == 0 {
			found.value = value
		} else { // key changes, delete and re-insert
			s.delete(found.key, found.value)
			newNode = s.insertNode(key, value)
		}
	} else {
		newNode = s.insertNode(key, value)
	}

	if newNode != nil {
		s.dict[value] = newNode
	}
	s.mu.Unlock()
	return found == nil
}

// Remove delete element specified by key
// Time complexity of this method is : O(log(N))
func (s *SortedSet) Remove(value interface{}) *Node {
	s.mu.Lock()
	found := s.dict[value]
	if found != nil {
		s.delete(found.key, found.value)
		s.mu.Unlock()
		return found
	}
	s.mu.Unlock()
	return nil
}

// GetByKeyRangeOptions special options
type GetByKeyRangeOptions struct {
	Limit        int  // limit the max nodes to return
	ExcludeStart bool // exclude start value, so it search in interval (start, end] or (start, end)
	ExcludeEnd   bool // exclude end value, so it search in interval [start, end) or (start, end)
	Remove       bool
}

// GetTop return top data
func (s *SortedSet) GetTop(count int, remove bool) (result []*Node) {
	return s.GetByRankRange(-1, -count, remove)
}

// GetRTop return top from end data
func (s *SortedSet) GetRTop(count int, remove bool) (result []*Node) {
	return s.GetByRankRange(1, count, remove)
}

// GetUntilKey get all values until given key
func (s *SortedSet) GetUntilKey(untilKey interface{}, remove bool) []interface{} {
	nodes := s.GetByKeyRange(s.emptyKey, untilKey, &GetByKeyRangeOptions{
		Remove: remove,
	})
	data := make([]interface{}, len(nodes))
	for i, nd := range nodes {
		data[i] = nd.Value()
	}
	return data
}

// GetByKeyRange get the nodes whose key within the specific range
// If options is nil, it `searches` in interval [start, end] without any limit by default
// Time complexity of this method is : O(log(N))
func (s *SortedSet) GetByKeyRange(start interface{}, end interface{}, options *GetByKeyRangeOptions) []*Node { //nolint:gocyclo
	s.mu.Lock()
	// prepare parameters
	limit := 2147483648
	if options != nil && options.Limit > 0 {
		limit = options.Limit
	}
	var remove bool
	if options != nil {
		remove = options.Remove
	}

	excludeStart := options != nil && options.ExcludeStart
	excludeEnd := options != nil && options.ExcludeEnd

	reverse := s.comparator(start, end) > 0
	if reverse {
		start, end = end, start
		excludeStart, excludeEnd = excludeEnd, excludeStart
	}

	var nodes []*Node

	// determine if out of range
	if s.length == 0 {
		s.mu.Unlock()
		return nodes
	}

	if reverse { // search from end to start
		x := s.header

		if excludeEnd {
			for i := s.level - 1; i >= 0; i-- {
				for x.level[i].forward != nil &&
					s.comparator(x.level[i].forward.key, end) < 0 {
					x = x.level[i].forward
				}
			}
		} else {
			for i := s.level - 1; i >= 0; i-- {
				for x.level[i].forward != nil &&
					s.comparator(x.level[i].forward.key, end) <= 0 {
					x = x.level[i].forward
				}
			}
		}

		for x != nil && limit > 0 {
			if excludeStart {
				if s.comparator(x.key, start) <= 0 {
					break
				}
			} else {
				if s.comparator(x.key, start) < 0 {
					break
				}
			}

			next := x.backward

			nodes = append(nodes, x)
			if remove {
				s.delete(x.Key(), x.Value())
			}
			limit--

			x = next
		}
	} else {
		// search from start to end
		x := s.header
		if excludeStart {
			for i := s.level - 1; i >= 0; i-- {
				for x.level[i].forward != nil &&
					s.comparator(x.level[i].forward.key, start) <= 0 {
					x = x.level[i].forward
				}
			}
		} else {
			for i := s.level - 1; i >= 0; i-- {
				for x.level[i].forward != nil &&
					s.comparator(x.level[i].forward.key, start) < 0 {
					x = x.level[i].forward
				}
			}
		}

		/* Current node is the last with key < or <= start. */
		x = x.level[0].forward

		for x != nil && limit > 0 {
			if excludeEnd {
				if s.comparator(x.key, end) >= 0 {
					break
				}
			} else {
				if s.comparator(x.key, end) > 0 {
					break
				}
			}

			next := x.level[0].forward

			nodes = append(nodes, x)
			if remove {
				s.delete(x.Key(), x.Value())
			}
			limit--

			x = next
		}
	}

	s.mu.Unlock()
	return nodes
}

// GetByRankRange get nodes within specific rank range [start, end]
// Note that the rank is 1-based integer. Rank 1 means the first node; Rank -1 means the last node;
// If start is greater than end, the returned array is in reserved order
// If remove is true, the returned nodes are removed
// Time complexity of this method is : O(log(N))
func (s *SortedSet) GetByRankRange(start int, end int, remove bool) []*Node {
	s.mu.Lock()
	/* Sanitize indexes. */
	if start < 0 {
		start = int(s.length) + start + 1
	}
	if end < 0 {
		end = int(s.length) + end + 1
	}
	if start <= 0 {
		start = 1
	}
	if end <= 0 {
		end = 1
	}

	reverse := start > end
	if reverse { // swap start and end
		start, end = end, start
	}

	var update [skiplistMaxlevel]*Node
	var nodes []*Node
	traversed := 0

	x := s.header
	for i := s.level - 1; i >= 0; i-- {
		for x.level[i].forward != nil &&
			traversed+int(x.level[i].span) < start {
			traversed += int(x.level[i].span)
			x = x.level[i].forward
		}
		if remove {
			update[i] = x
		} else if traversed+1 == start {
			break
		}
	}

	traversed++
	x = x.level[0].forward
	for x != nil && traversed <= end {
		next := x.level[0].forward

		nodes = append(nodes, x)
		if remove {
			s.deleteNode(x, update)
		}

		traversed++
		x = next
	}

	if reverse {
		for i, j := 0, len(nodes)-1; i < j; i, j = i+1, j-1 {
			nodes[i], nodes[j] = nodes[j], nodes[i]
		}
	}
	s.mu.Unlock()
	return nodes
}

// GetByRank get  node by rank.
// Note that the rank is 1-based integer. Rank 1 means the first node; Rank -1 means the last node;
// If remove is true, the returned nodes are removed
// If node is not found at specific rank, nil is returned
// Time complexity of this method is : O(log(N))
func (s *SortedSet) GetByRank(rank int, remove bool) *Node {
	nodes := s.GetByRankRange(rank, rank, remove)
	if len(nodes) == 1 {
		return nodes[0]
	}
	return nil
}

// GetByValue get node by value
// If node is not found, nil is returned
// Time complexity : O(1)
func (s *SortedSet) GetByValue(value interface{}) *Node {
	s.mu.RLock()
	n := s.dict[value]
	s.mu.RUnlock()
	return n
}

type Dump struct {
	KeyType string
	Data    map[string]interface{}
}

// Dump generate dump of data
func (s *SortedSet) Dump(keyDump func(key interface{}) (string, error)) (string, error) {
	s.mu.RLock()
	data := map[string][]interface{}{}
	for _, v := range s.dict {
		strKey, err := keyDump(v.Key())
		if err != nil {
			return "", err
		}
		data[strKey] = append(data[strKey], v.Value())
	}
	s.mu.RUnlock()
	dump, err := json.Marshal(data)
	return string(dump), err
}

// Restore restore dump
func (s *SortedSet) Restore(keyRestore func(key string) (interface{}, error), dump string) error {
	var data map[string][]interface{}
	err := json.Unmarshal([]byte(dump), &data)
	if err != nil {
		return err
	}
	for key, values := range data {
		rkey, err := keyRestore(key)
		if err != nil {
			return err
		}
		for _, value := range values {
			s.Upsert(rkey, value)
		}
	}
	return nil
}

// Contains return true if value exists
func (s *SortedSet) Contains(value interface{}) bool {
	return s.GetByValue(value) != nil
}

// FindRank find the rank of the node specified by key
// Note that the rank is 1-based integer. Rank 1 means the first node
// If the node is not found, 0 is returned. Otherwise rank(> 0) is returned
// Time complexity of this method is : O(log(N))
func (s *SortedSet) FindRank(value interface{}) int {
	s.mu.RLock()
	rank := 0
	node := s.dict[value]
	if node != nil {
		x := s.header
		for i := s.level - 1; i >= 0; i-- {
			for x.level[i].forward != nil &&
				(s.comparator(x.level[i].forward.key, node.key) < 0 ||
					(s.comparator(x.level[i].forward.key, node.key) == 0 &&
						x.level[i].forward.value != node.value)) {
				rank += int(x.level[i].span)
				x = x.level[i].forward
			}

			if x.value == value {
				s.mu.RUnlock()
				return rank
			}
		}
	}
	s.mu.RUnlock()
	return 0
}

// SortedSetLevel describe sorted set level
type Level struct {
	forward *Node
	span    uint64
}

// SortedSetNode node in skip list
type Node struct {
	value    interface{} // associated data
	key      interface{} // key to determine the order of this node in the set
	backward *Node
	level    []Level
}

// Value return the key of the node
func (s *Node) Value() interface{} {
	return s.value
}

// Key return the node of the node
func (s *Node) Key() interface{} {
	return s.key
}
