package registry

import "errors"

// All kinds of errors for registry
var (
	ErrNotFoundEntity = errors.New("not found entity")
	ErrCastingData    = errors.New("cannot cast type")
)
