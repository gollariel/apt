package registry

import (
	"sync"

	"bitbucket.org/espinpro/go-engine/registry/comparator"

	"bitbucket.org/espinpro/go-engine/registry/sortedset"

	"bitbucket.org/espinpro/go-engine/geometry/shapes"
)

// RTree options
const (
	minRTreeOption = 25
	maxRTreeOption = 50
)

// Group contains all entities for given type
type Group struct {
	entities  map[interface{}]interface{}
	rtree     *RTree
	sortedSet *sortedset.SortedSet
	index     *BidirectionalIndex
	mu        sync.RWMutex
}

// NewGroup return group
func NewGroup() *Group {
	return &Group{
		entities:  make(map[interface{}]interface{}),
		rtree:     NewRTree(minRTreeOption, maxRTreeOption),
		sortedSet: sortedset.NewSortedSet(uint64(0), comparator.UInt64Comparator),
		index:     NewBidirectionalIndex(),
	}
}

// Set set new entity
func (g *Group) Set(id interface{}, e interface{}) (err error) {
	g.mu.Lock()
	g.entities[id] = e
	g.mu.Unlock()

	c, ok := e.(Constructable)
	if ok {
		err = c.Construct()
		if err != nil {
			return err
		}
	}

	s, ok := e.(shapes.Spatial)
	if ok {
		g.rtree.Insert(s)
	}

	return
}

// Get get entity
func (g *Group) Get(id interface{}) (e interface{}, err error) {
	g.mu.RLock()
	var exists bool
	if e, exists = g.entities[id]; !exists {
		err = ErrNotFoundEntity
	}
	g.mu.RUnlock()
	return
}

// Count count all entities
func (g *Group) Count() int {
	g.mu.RLock()
	l := len(g.entities)
	g.mu.RUnlock()
	return l
}

// CallWithLock call with Lock
func (g *Group) CallWithLock(f func(i *BidirectionalIndex, d map[interface{}]interface{}, r *RTree, s *sortedset.SortedSet) (e interface{}, err error)) (interface{}, error) {
	g.mu.Lock()
	e, err := f(g.index, g.entities, g.rtree, g.sortedSet)
	g.mu.Unlock()
	return e, err
}

// Delete delete entity
func (g *Group) Delete(id interface{}) (err error) {
	g.mu.Lock()
	e, exists := g.entities[id]
	delete(g.entities, id)
	g.mu.Unlock()

	if exists {
		destroyable, ok := e.(Destroyable)
		if ok {
			err = destroyable.Destroy()
		}

		s, ok := e.(shapes.Spatial)
		if ok {
			g.rtree.Delete(s)
		}
	}
	return
}

// GetAll get all entities
func (g *Group) GetAll() []interface{} {
	g.mu.RLock()
	result := make([]interface{}, len(g.entities))
	var i int
	for _, entity := range g.entities {
		result[i] = entity
		i++
	}
	g.mu.RUnlock()
	return result
}

// GetAllIDs get all ids
func (g *Group) GetAllIDs() []interface{} {
	g.mu.RLock()
	result := make([]interface{}, len(g.entities))
	var i int
	for id := range g.entities {
		result[i] = id
		i++
	}
	g.mu.RUnlock()
	return result
}

// GetGenerator get all via channel
func (g *Group) GetGenerator() chan interface{} {
	result := make(chan interface{}, bufferSize)
	go func() {
		g.mu.RLock()
		for _, entity := range g.entities {
			result <- entity
		}
		close(result)
		g.mu.RUnlock()
	}()
	return result
}

// Clear clear entities
func (g *Group) Clear() {
	g.mu.Lock()
	g.entities = make(map[interface{}]interface{})
	g.mu.Unlock()
}

// Tick call tick
func (g *Group) Tick(key interface{}) {
	g.mu.RLock()
	for _, entity := range g.entities {
		n, ok := entity.(Ticker)
		if ok {
			n.Tick()
		}
	}
	g.mu.RUnlock()
}

type searchData struct {
	id   interface{}
	data interface{}
}

// Search search entities
func (g *Group) Search(key interface{}, result chan interface{}, f SearchFunction) {
	entities := make(chan searchData, bufferSize)

	var wg sync.WaitGroup
	wg.Add(workersCount)
	for i := 0; i < workersCount; i++ {
		go func(result chan interface{}, f SearchFunction) {
			for n := range entities {
				if f == nil || f(key, n.id, n.data) {
					result <- n.data
				}
			}
			wg.Done()
		}(result, f)
	}

	g.mu.RLock()
	for id, entity := range g.entities {
		entities <- searchData{
			id:   id,
			data: entity,
		}
	}
	g.mu.RUnlock()

	close(entities)
	wg.Wait()
}

// SearchOne search one entuty
func (g *Group) SearchOne(key interface{}, f SearchFunction) (d interface{}) {
	g.mu.RLock()
	for id, data := range g.entities {
		if f == nil || f(key, id, data) {
			d = data
			break
		}
	}
	g.mu.RUnlock()
	return
}

// GetRTree return rtree index
func (g *Group) GetRTree() *RTree {
	return g.rtree
}

// GetSortedSet return sorted set
func (g *Group) GetSortedSet() *sortedset.SortedSet {
	return g.sortedSet
}

// GetBidirectionalIndex return bidirectional index
func (g *Group) GetBidirectionalIndex() *BidirectionalIndex {
	return g.index
}

// SafeCall safe call in group
func (g *Group) SafeCall(f func(map[interface{}]interface{}, *RTree, *sortedset.SortedSet, *BidirectionalIndex)) {
	g.mu.Lock()
	f(g.entities, g.rtree, g.sortedSet, g.index)
	g.mu.Unlock()
}
