package registry

import "sync"

var (
	multiRegistry = map[interface{}]*Registry{}
	mu            sync.RWMutex
)

// Get return registry by key
func Get(k interface{}) *Registry {
	mu.RLock()
	v, ok := multiRegistry[k]
	mu.RUnlock()
	if !ok {
		mu.Lock()
		v = NewRegistry()
		multiRegistry[k] = v
		mu.Unlock()
	}
	return v
}

// ResetByKey recreate storage
func ResetByKey(k interface{}) {
	mu.Lock()
	multiRegistry[k] = NewRegistry()
	mu.Unlock()
}

// DeleteByKey delete storage
func DeleteByKey(k interface{}) {
	mu.Lock()
	delete(multiRegistry, k)
	mu.Unlock()
}
