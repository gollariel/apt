package registry

import (
	"sync"

	"bitbucket.org/espinpro/go-engine/geometry/shapes"
)

// Grid grid index
type Grid struct {
	index map[int]map[int]map[int]shapes.Spatial
	maxX  int
	maxY  int
	maxZ  int
	mu    sync.RWMutex
}

// NewGrid return new index
func NewGrid() *Grid {
	return &Grid{
		index: map[int]map[int]map[int]shapes.Spatial{},
	}
}

// Add add data to index
func (g *Grid) Add(x, y, z int, s shapes.Spatial) {
	g.mu.Lock()
	if x > g.maxX {
		g.maxX = x
	}
	if y > g.maxY {
		g.maxY = y
	}
	if z > g.maxZ {
		g.maxZ = y
	}
	g.index[x][y][z] = s
	g.mu.Unlock()
}

// Get get from cell
func (g *Grid) Get(x, y, z int) (v shapes.Spatial) {
	g.mu.RLock()
	if d, ok := g.index[x][y][z]; ok {
		v = d
	}
	g.mu.RUnlock()
	return
}
