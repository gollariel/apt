package server

import (
	"context"
	"net/http"
	"strings"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/utils"
)

const separator = "/"

// Default mux configs
const (
	MethodAny string           = "any"
	CtxValues utils.ContextKey = "values"
)

// Route simple http mux
type Route struct {
	method  string
	handler http.Handler
}

// NewRoute return new route
func NewRoute(method string, handler http.Handler) *Route {
	return &Route{
		method:  method,
		handler: handler,
	}
}

// Index index for routes
type Index struct {
	index    []*Index
	name     string
	wildcard bool
	global   bool
	routes   map[string]http.Handler
}

// Root return root index
func Root(routes ...*Route) *Index {
	routesMap := map[string]http.Handler{}
	for _, r := range routes {
		if r.method == "" {
			r.method = MethodAny
		}
		routesMap[r.method] = r.handler
	}

	return &Index{
		index:    []*Index{},
		name:     "",
		wildcard: false,
		global:   false,
		routes:   routesMap,
	}
}

func newIndex(name string, index []*Index, routes ...*Route) *Index {
	var wildcard, global bool
	if len(name) > 0 {
		if name[0:1] == ":" {
			name = name[1:]
			wildcard = true
		} else if name[0:1] == "*" {
			name = "*"
			wildcard = true
			global = true
		}
	}

	routesMap := map[string]http.Handler{}
	for _, r := range routes {
		if r.method == "" {
			r.method = MethodAny
		}
		routesMap[r.method] = r.handler
	}

	return &Index{
		index:    index,
		name:     name,
		wildcard: wildcard,
		global:   global,
		routes:   routesMap,
	}
}

// Add add path to index
func (i *Index) Add(path string, routes ...*Route) {
	path = strings.Trim(strings.Trim(path, separator), "")
	parts := strings.Split(path, separator)
	ci := i
	for _, name := range parts[:len(parts)-1] {
		cin, exists := ci.chooseIndex(name)
		if !exists {
			ci.index = append(ci.index, newIndex(name, []*Index{}))
			ci = ci.index[len(ci.index)-1]
		} else {
			ci = ci.index[cin]
		}
	}
	name := parts[len(parts)-1]
	cin, exists := ci.chooseIndex(name)
	if !exists {
		ci.index = append(ci.index, newIndex(name, []*Index{}, routes...))
	} else {
		ci.index[cin] = newIndex(name, ci.index[cin].index, routes...)
	}
}

// Get get handler and values by method and path
func (i *Index) Get(method, path string) (http.Handler, map[string]string, bool) {
	path = strings.Trim(strings.Trim(path, separator), "")
	parts := strings.Split(path, separator)

	values := map[string]string{}
	ci := i
	for _, p := range parts {
		v := ci.choose(p)

		if v != nil { //nolint:gocritic
			if v.wildcard {
				values[v.name] = p
			}
			ci = v
		} else if ci.global {
			values["*"] = strings.Join([]string{values["*"], p}, ",")
			continue
		} else {
			return nil, values, false
		}
	}

	m, e := ci.routes[method]
	if !e {
		m, e = ci.routes[MethodAny]
	}
	return m, values, e
}

func (i *Index) choose(name string) *Index {
	if len(name) > 0 && name[0:1] == ":" {
		name = name[1:]
	}
	for _, p := range i.index {
		if p.name == name {
			return p
		}
	}
	for _, p := range i.index {
		if p.wildcard {
			return p
		}
	}
	return nil
}

func (i *Index) chooseIndex(name string) (int, bool) {
	if len(name) > 0 && name[0:1] == ":" {
		name = name[1:]
	}
	for i, p := range i.index {
		if p.name == name {
			return i, true
		}
	}
	return 0, false
}

// Handler return mux handler
func (i *Index) Handler(defaultHandler http.HandlerFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler, values, ok := i.Get(r.Method, r.URL.Path)
		if !ok {
			if defaultHandler != nil {
				defaultHandler(w, r)
			} else {
				fastlog.Errorw("Not found resource", "method", r.Method, "path", r.URL.Path)
				http.Error(w, "Not found resource", http.StatusNotFound)
			}
			return
		}
		r = r.WithContext(context.WithValue(r.Context(), CtxValues, values))
		handler.ServeHTTP(w, r)
	})
}
