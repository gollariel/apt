package server

import (
	"encoding/json"
	"net/http"
	"time"
)

// Contains names of header attributes
const (
	HeaderAttributeContentType = "Content-Type"
)

// Contains any available content types values
const (
	contentTypeJSON = "application/json; charset=utf-8"
)

// Contains key for error inside content
const (
	contentKeyError = "error"
)

// Content Describe content to return to client
type Content map[string]interface{}

// NewContent make new content by given key values
func NewContent(keyValues ...interface{}) (Content, error) {
	var data Content
	if len(keyValues)%2 == 0 {
		data = make(Content, len(keyValues)/2) // nolint:gomnd
		for i := 0; i < len(keyValues); i += 2 {
			key, ok := keyValues[i].(string)
			if !ok {
				return nil, ErrWrongValue
			}
			data[key] = keyValues[i+1]
		}
	} else {
		return nil, ErrNotEnoughArguments
	}
	return data, nil
}

// Response contains base response for every request
type Response struct {
	Meta    *Meta   `json:"meta,omitempty"`
	Content Content `json:"content"`
	Details Details `json:"details"`
	status  int
	header  map[string]string
}

// Meta describe meta information as a pagination
type Meta struct {
	Total  uint64 `json:"total"`
	Offset uint64 `json:"offset"`
	Count  uint64 `json:"count"`
	Next   string `json:"next"`
}

// NewMeta return new meta object
func NewMeta(total, offset, count uint64, next string) *Meta {
	return &Meta{
		Total:  total,
		Offset: offset,
		Count:  count,
		Next:   next,
	}
}

// Details contains server side details
type Details struct {
	Time      time.Time `json:"time"`
	RequestID string    `json:"request_id"`
}

// NewResponse return new response by given request id
func NewResponse(requestID string) *Response {
	r := &Response{
		Content: map[string]interface{}{},
		Details: Details{
			Time:      time.Now(),
			RequestID: requestID,
		},
		header: map[string]string{},
	}
	return r
}

// SetStatusCode set response status code
func (r *Response) SetStatusCode(status int) *Response {
	r.status = status
	return r
}

// Header set new header value
func (r *Response) Header(key string, value string) *Response {
	r.header[key] = value
	return r
}

// SetContentType set content type
func (r *Response) SetContentType(contentType string) *Response {
	return r.Header(HeaderAttributeContentType, contentType)
}

// SetMeta set meta information to response
func (r *Response) SetMeta(meta *Meta) *Response {
	r.Meta = meta
	return r
}

// Add add specific data to response
func (r *Response) Add(key string, value interface{}) *Response {
	r.Content[key] = value
	return r
}

// Error set error to response
func (r *Response) Error(value error) *Response {
	r.Content[contentKeyError] = value.Error()
	return r
}

// Set set content (content should be empty)
func (r *Response) Set(content Content) error {
	if len(r.Content) > 0 {
		return ErrContentIsNotEmpty
	}
	r.Content = content
	return nil
}

// SetTime set server side time
func (r *Response) SetTime(d time.Time) *Response {
	r.Details.Time = d
	return r
}

// SendJSON send response as json
func (r *Response) SendJSON(w http.ResponseWriter) error {
	data, err := json.Marshal(r)
	if err != nil {
		return err
	}
	r.SetContentType(contentTypeJSON)
	for k, v := range r.header {
		w.Header().Add(k, v)
	}
	w.WriteHeader(r.status)
	_, err = w.Write(data)
	if err != nil {
		return err
	}
	return nil
}
