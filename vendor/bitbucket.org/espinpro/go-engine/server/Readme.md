### Middleware

Middleware it`s just a wrapper for manage any kind of chain of middlewares. It's very useful in cases of middlewares, what should support additional params.



### Create new middleware

Each middleware should follow the `Chain` type. Chain it's closure function what expect to receive next handler and return previous handler: `func(h Handler) Handler`
For example:

```go
func ExampleMiddleware(next route.Handler) route.Handler {
	return func(context *controller.RootContext, w web.ResponseWriter, r *web.Request) {
		// TODO Add some logic here
		next(context, w, r)
	}
}
```

All logic of middleware should be places inside returned function. That logic will be executed for each request.

### Usage

To start using our new middleware in the API handler, we should create new middleware  chain, and put our middleware inside that chain:

```go
m := NewMiddleware(ExampleMiddleware)
```

or

```go
m := NewMiddleware()
m.AddChain(ExampleMiddleware)
```

Each middleware chain support as many as we want chains.



Then when we init api handler, we should wrap that handler with `Then` method:

```go
m.Then(SpecialHandler) // return valid handler
```

### Middleware with additional options

We can use that middleware with additional arguments as well. For achieve that, we have two options:
1) Add special function:

```go
type MiddlewareExampleWithOptions struct {
	Option1 string
}

func (m *MiddlewareExampleWithOptions) ExampleMiddleware(next route.Handler) route.Handler {
	return func(context *controller.RootContext, w web.ResponseWriter, r *web.Request) {
		// TODO Add some logic here
		// use m.Option1
		next(context, w, r)
	}
}

func MiddlewareWithArguments(m *route.Middleware, handler route.Handler, option1 string) route.Handler {
	md := &MiddlewareExampleWithOptions{
		Option1: option1
	}
	m.AddChain(md.ExampleMiddleware)
	return rm.Then(handler)
}

// For use:
m := NewMiddleware()
MiddlewareWithArguments(m, SpecialHandler, "test")
```

2) Add wrapper:

```go
func MiddlewareWithArguments(option1 string) Chain {
	md := &MiddlewareExampleWithOptions{
		Option1: option1
	}
	return md.ExampleMiddleware
}

// usage:
m := NewMiddleware()
m.AddChain(MiddlewareWithArguments("test"))
m.Then(SpecialHandler) // handler to use in router
```

