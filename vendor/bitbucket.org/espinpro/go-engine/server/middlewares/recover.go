package middlewares

import (
	"net/http"

	"bitbucket.org/espinpro/go-engine/fastlog"
)

// Recover recover after panic
func Recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rval := recover(); rval != nil {
				fastlog.Errorw("Recovered request panic", "rval", rval)
				http.Error(w, "Panic during the request", http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}
