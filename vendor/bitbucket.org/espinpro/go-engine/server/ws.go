package server

import (
	"context"
	"io"
	"net"
	"net/http"

	"bitbucket.org/espinpro/go-engine/ecs"

	"bitbucket.org/espinpro/go-engine/commands"
	"bitbucket.org/espinpro/go-engine/engine/communications"
	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/registry"
	"bitbucket.org/espinpro/go-engine/utils"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

// Events for change behavior
const (
	EventNewUser         = "event:new_user"
	EventCloseConnection = "event:close_connection"
)

// Keys for using in context
var (
	KeyPID           utils.ContextKey = "key:pid"
	KeyCommunication utils.ContextKey = "key:communication"
)

// WSHandler websocket handler
type WSHandler struct {
	defaultOpCode ws.OpCode
}

// NewWSHandler return new ws handler
func NewWSHandler(defaultOpCode ws.OpCode) *WSHandler {
	return &WSHandler{
		defaultOpCode: defaultOpCode,
	}
}

// WS process ws connection
func (h *WSHandler) WS(w http.ResponseWriter, r *http.Request) {
	pid, ok := r.Context().Value(ecs.RegistryName).(uint64)
	if !ok {
		pid = registry.Get(ecs.RegistryName).NextID()
	}

	u := ws.HTTPUpgrader{
		Header: w.Header(),
	}
	conn, _, _, err := u.Upgrade(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	go func() {
		communication := communications.NewCommunicateComponent(conn)

		ctx := context.WithValue(context.Background(), KeyPID, pid)
		ctx = context.WithValue(ctx, KeyCommunication, communication)
		commands.GetEventManager().Call(ctx, EventNewUser)

		defer func() {
			if err := communication.Close(); err != nil {
				fastlog.Errorw("Errorw in destroy player", "error", err)
			}

			commands.GetEventManager().Call(ctx, EventCloseConnection)
		}()

		go func() {
			for msg := range communication.GetOutgoing() {
				err = h.write(conn, msg)
				if err != nil {
					fastlog.Errorw("Errorw in write client data", "error", err)
					break
				}
			}
		}()

		for {
			err := h.read(conn, communication)
			if err != nil {
				break
			}
		}
	}()
}

func (h *WSHandler) read(c net.Conn, communication communications.Communication) (err error) {
	defer func() {
		if r := recover(); r != nil {
			fastlog.Errorw("Recovered in read", "panic", r)
			err = ErrPanicInRead
		}
	}()

	msg, _, err := wsutil.ReadClientData(c)
	if err == io.EOF {
		return err
	} else if err != nil {
		fastlog.Errorw("Error in read client data", "error", err)
		return err
	}
	communication.GetIncoming() <- msg

	return err
}

func (h *WSHandler) write(c net.Conn, msg []byte) (err error) {
	defer func() {
		if r := recover(); r != nil {
			fastlog.Errorw("Recovered in write", "panic", r)
			err = ErrPanicInWrite
		}
	}()

	return wsutil.WriteServerMessage(c, h.defaultOpCode, msg)
}
