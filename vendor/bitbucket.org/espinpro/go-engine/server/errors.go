package server

import "errors"

// Server errors
var (
	ErrPanicInRead        = errors.New("panic in read")
	ErrPanicInWrite       = errors.New("panic in write")
	ErrNotEnoughArguments = errors.New("error not enough arguments")
	ErrWrongValue         = errors.New("error wrong value")
	ErrContentIsNotEmpty  = errors.New("content is not empty")
)
