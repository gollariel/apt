package server

import (
	"net/http"
)

// Middleware contains methods to call before handle request
type Middleware struct {
	chains []Chain
}

// Chain single chain
type Chain func(h http.Handler) http.Handler

// NewMiddleware return new middleware
func NewMiddleware(c ...Chain) *Middleware {
	return &Middleware{
		chains: c,
	}
}

// AddChain add middleware to execute
func (m *Middleware) AddChain(c Chain) {
	m.chains = append(m.chains, c)
}

// Then return router handler
func (m *Middleware) Then(h http.Handler) http.Handler {
	for i := range m.chains {
		h = m.chains[len(m.chains)-1-i](h)
	}
	return h
}

// Copy return copied chain
func (m *Middleware) Copy() *Middleware {
	return NewMiddleware(m.chains...)
}

// Merge merge middlewares into current
func (m *Middleware) Merge(middlewares ...*Middleware) {
	for _, middleware := range middlewares {
		for _, chain := range middleware.chains {
			m.AddChain(chain)
		}
	}
}
