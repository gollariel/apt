package commands

import (
	"context"
	"sync"
	"sync/atomic"

	"bitbucket.org/espinpro/go-engine/registry"

	"bitbucket.org/espinpro/go-engine/fastlog"
)

const (
	minWorkersCount     = 1
	maxWorkersCount     = 5
	defaultWorkersCount = 4
)

// RegistryName name of specific registry instance
const RegistryName = "commands"

// EventHandlerFunc event handler like function
type EventHandlerFunc func(ctx context.Context)

// Handle for event handler function
func (f EventHandlerFunc) Handle(ctx context.Context) {
	f(ctx)
}

// EventHandler describe event handler
type EventHandler interface {
	Handle(context.Context)
}

// EventManager event manager
type EventManager struct {
	aid         uint64
	subscribers map[interface{}]map[uint64]EventHandler
	workers     int
	mu          sync.RWMutex
}

// NewEventManager return new event manager
func NewEventManager(workers int) *EventManager {
	if workers < minWorkersCount || workers > maxWorkersCount {
		workers = minWorkersCount
	}
	return &EventManager{
		subscribers: make(map[interface{}]map[uint64]EventHandler),
		workers:     workers,
	}
}

// NextID return next id
func (e *EventManager) NextID() uint64 {
	return atomic.AddUint64(&e.aid, 1)
}

// Subscribe subscribe on event
func (e *EventManager) Subscribe(event interface{}, handler EventHandler) uint64 {
	e.mu.Lock()
	if _, ok := e.subscribers[event]; !ok {
		e.subscribers[event] = make(map[uint64]EventHandler)
	}
	id := registry.Get(RegistryName).NextID()
	e.subscribers[event][id] = handler
	e.mu.Unlock()
	return id
}

// Unsubscribe unsubscribe from event
func (e *EventManager) Unsubscribe(event interface{}, id uint64) {
	e.mu.Lock()
	if _, ok := e.subscribers[event]; ok {
		delete(e.subscribers[event], id)
	}
	e.mu.Unlock()
}

// GetHandlers return all handlers for event
func (e *EventManager) GetHandlers(event interface{}) []EventHandler {
	e.mu.RLock()
	if _, ok := e.subscribers[event]; !ok {
		e.mu.RUnlock()
		return []EventHandler{}
	}

	var i int
	handlers := make([]EventHandler, len(e.subscribers[event]))
	for _, handler := range e.subscribers[event] {
		handlers[i] = handler
		i++
	}

	e.mu.RUnlock()
	return handlers
}

// Call call event
func (e *EventManager) Call(ctx context.Context, event interface{}) {
	defer func() {
		if rval := recover(); rval != nil {
			fastlog.Errorw("Recovered request panic", "rval", rval)
		}
	}()

	handlers := e.GetHandlers(event)
	if len(handlers) == 0 {
		return
	}

	var wg sync.WaitGroup
	wg.Add(e.workers)
	handler := make(chan EventHandler)

	for i := 0; i < e.workers; i++ {
		go func() {
			defer wg.Done()
			for {
				select {
				case <-ctx.Done():
					return
				case s := <-handler:
					if s == nil {
						return
					}
					s.Handle(ctx)
				}
			}
		}()
	}

	for _, s := range handlers {
		handler <- s
	}
	close(handler)
	wg.Wait()
}

var eventManager = NewEventManager(defaultWorkersCount)

// GetEventManager return default event manager
func GetEventManager() *EventManager {
	return eventManager
}
