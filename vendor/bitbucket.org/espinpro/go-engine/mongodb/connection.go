package mongodb

import (
	"sync"

	"bitbucket.org/espinpro/go-engine/fastlog"
)

var (
	connection *MongoClient
	mu         sync.RWMutex
)

// Set set global connection
func Set(r *MongoClient) {
	mu.Lock()
	connection = r
	mu.Unlock()
}

// Get return mongo connection
func Get() (*MongoClient, error) {
	mu.RLock()
	defer mu.RUnlock()

	if connection == nil {
		return nil, ErrConnectionIsNotSet
	}
	return connection, nil
}

// Default return default client
func Default() (*MongoClient, error) {
	c, err := Get()
	if err != nil {
		config, err := GetConnectionConfigFromEnv()
		if err != nil {
			fastlog.Errorw("Error getting mongo config", "err", err)
			return nil, err
		}
		c, err = NewMongoClient(config)
		if err != nil {
			fastlog.Errorw("Error getting mongo client", "err", err)
			return nil, err
		}
		Set(c)
	}
	return c, nil
}
