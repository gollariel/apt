package mongodb

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

// EnvPrefix environment prefix for mongodb config
const EnvPrefix = "MONGO"

// ConnectionConfig contains required data for mongo
type ConnectionConfig struct {
	URI      string        `required:"true"`
	Timeout  time.Duration `required:"true"`
	Database string        `required:"true"`
}

// NewMongoDBConfig return new mongo config
func NewMongoDBConfig(uri string, timeout time.Duration, database string) *ConnectionConfig {
	return &ConnectionConfig{
		URI:      uri,
		Timeout:  timeout,
		Database: database,
	}
}

// GetConnectionConfigFromEnv return mongodb configs bases on environment variables
func GetConnectionConfigFromEnv() (*ConnectionConfig, error) {
	c := new(ConnectionConfig)
	err := envconfig.Process(EnvPrefix, c)
	return c, err
}
