package mongodb

import (
	"context"
	"reflect"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoClient client for mongo db
type MongoClient struct {
	*mongo.Database
}

// NewMongoClient return client from config
func NewMongoClient(config *ConnectionConfig) (*MongoClient, error) {
	ctx := context.Background()
	if config.Timeout > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, config.Timeout)
		defer cancel()
	}

	cs := options.Client().ApplyURI(config.URI)
	client, err := mongo.Connect(ctx, cs)
	if err != nil {
		return nil, err
	}

	return &MongoClient{
		Database: client.Database(config.Database),
	}, nil
}

// FindOne find and decode one element
func (m *MongoClient) FindOne(ctx context.Context, collection string, value interface{}, filter interface{}, opts ...*options.FindOneOptions) error {
	result := m.Collection(collection).FindOne(ctx, filter, opts...)
	return result.Decode(value)
}

func (m *MongoClient) retrieve(ctx context.Context, cur *mongo.Cursor, value interface{}) ([]interface{}, error) {
	var err error
	defer func() {
		if closeErr := cur.Close(ctx); closeErr != nil {
			err = closeErr
		}
	}()
	var result []interface{}
	for cur.Next(ctx) {
		valueLocal := Clone(value)
		err := cur.Decode(valueLocal)
		if err != nil {
			return nil, err
		}
		v := reflect.Indirect(reflect.ValueOf(valueLocal))
		item := v.Interface()

		result = append(result, item)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	return result, err
}

func Clone(oldObj interface{}) interface{} {
	newObj := reflect.New(reflect.TypeOf(oldObj).Elem())
	oldVal := reflect.ValueOf(oldObj).Elem()
	newVal := newObj.Elem()
	for i := 0; i < oldVal.NumField(); i++ {
		newValField := newVal.Field(i)
		if newValField.CanSet() {
			newValField.Set(oldVal.Field(i))
		}
	}

	return newObj.Interface()
}

// Find find all and return
func (m *MongoClient) Find(ctx context.Context, collection string, value interface{}, filter interface{}, opts ...*options.FindOptions) ([]interface{}, error) {
	cur, err := m.Collection(collection).Find(ctx, filter, opts...)
	if err != nil {
		return nil, err
	}
	return m.retrieve(ctx, cur, value)
}

// FindOneByID return value by _id
func (m *MongoClient) FindOneByID(ctx context.Context, collection string, value interface{}, id primitive.ObjectID, opts ...*options.FindOneOptions) error {
	filter := bson.D{{Key: "_id", Value: id}}
	result := m.Collection(collection).FindOne(ctx, filter, opts...)
	return result.Decode(value)
}

// CountDocuments count documents
func (m *MongoClient) CountDocuments(ctx context.Context, collection string, filter interface{}, opts ...*options.CountOptions) (int64, error) {
	count, err := m.Collection(collection).CountDocuments(ctx, filter, opts...)
	if err != nil {
		return 0, err
	}
	return count, nil
}

// Aggregate aggregate rows
func (m *MongoClient) Aggregate(ctx context.Context, collection string, value interface{}, pipeline interface{}, opts ...*options.AggregateOptions) ([]interface{}, error) {
	cur, err := m.Collection(collection).Aggregate(ctx, pipeline, opts...)
	if err != nil {
		return nil, err
	}
	return m.retrieve(ctx, cur, value)
}

// InsertOne insert single object
func (m *MongoClient) InsertOne(ctx context.Context, collection string, value interface{}, opts ...*options.InsertOneOptions) error {
	_, err := m.Collection(collection).InsertOne(ctx, value, opts...)
	return err
}

// UpsertOne insert or update single object
func (m *MongoClient) UpsertOne(ctx context.Context, collection string, update interface{}, filter interface{}, opts ...*options.UpdateOptions) error {
	o := options.Update()
	o.SetUpsert(true)
	opts = append(opts, o)

	_, err := m.Collection(collection).UpdateOne(ctx, filter, update, opts...)
	return err
}

// UpdateByObject update to an object by filter
func (m *MongoClient) UpdateByObject(ctx context.Context, collection string, value interface{}, filter interface{}, opts ...*options.UpdateOptions) error {
	o := options.Update()
	o.SetUpsert(true)
	opts = append(opts, o)

	update := bson.D{{Key: "$set", Value: value}}
	_, err := m.Collection(collection).UpdateOne(ctx, filter, update, opts...)
	return err
}

// DeleteOne delete one element
func (m *MongoClient) DeleteOne(ctx context.Context, collection string, filter interface{}, opts ...*options.DeleteOptions) error {
	_, err := m.Collection(collection).DeleteOne(ctx, filter, opts...)
	return err
}

// Drop drop collection
func (m *MongoClient) Drop(ctx context.Context, collection string) error {
	return m.Collection(collection).Drop(ctx)
}
