package utils

import (
	"math"
)

// mathematics constants
const (
	DefaultPrecision float64 = 0.0001
)

// RoundWithPrecision round precision
func RoundWithPrecision(value float64, precision float64) float64 {
	if ApproximatelyEqual(precision, 0) {
		precision = DefaultPrecision
	}
	precision = 1 / precision
	return math.Round(value*precision) / precision
}

// Clamp return the result of the "value" clamped by
func Clamp(value, lowerLimit, upperLimit float64) float64 {
	if value < lowerLimit {
		return lowerLimit
	} else if value > upperLimit {
		return upperLimit
	}
	return value
}

// ApproximatelyEqual function to test if two real numbers are (almost) equal
func ApproximatelyEqual(a, b float64) bool {
	epsilon := math.SmallestNonzeroFloat64
	difference := a - b
	return difference < epsilon && difference > -epsilon
}
