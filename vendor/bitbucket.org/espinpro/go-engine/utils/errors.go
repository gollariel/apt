package utils

import "errors"

// All kind of errors
var (
	ErrRetryWithContinue = errors.New("error retry with continue")
)
