package mathutils

import (
	"math/rand"
)

// Throttle represents throttle
type Throttle struct {
	intn     func(int) int
	float64n func() float64
}

// NewThrottle initiates Throttle
func NewThrottle() *Throttle {
	return &Throttle{
		intn:     rand.Intn,
		float64n: rand.Float64,
	}
}

// Throttle throttles factor based on given base number
func (t *Throttle) Throttle(base, factor int) bool {
	return throttle(t.intn, base, factor)
}

// throttle throttles given factor based on given base number and intn func
func throttle(intn func(int) int, base, factor int) bool {
	switch {
	case factor <= 0:
		return false
	case base <= factor:
		return true
	case base <= 0:
		return false
	default:
		return intn(base-1) < factor
	}
}
