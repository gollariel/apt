package mathutils

import (
	"hash/crc32"
)

// Crc32Mod gets crc32 checksum by given str and return module of the checksum
func Crc32Mod(str string, modulus uint32) uint32 {
	if modulus == 0 {
		return 0
	}
	return crc32.ChecksumIEEE([]byte(str)) % modulus
}
