package mathutils

import (
	"math"
)

// mathematics constants
const (
	DefaultPrecision float64 = 0.0001
	MachineEpsilon   float64 = 0.000001
)

// RoundSliceWithPrecision round slice values with precision
func RoundSliceWithPrecision(value []float64, precision float64) []float64 {
	result := make([]float64, len(value))
	for i, v := range value {
		result[i] = RoundWithPrecision(v, precision)
	}
	return result
}

// SumValues sum values
func SumValues(value []float64) (result float64) {
	for _, v := range value {
		result += v
	}
	return result
}

// RoundWithPrecision round precision
func RoundWithPrecision(value float64, precision float64) float64 {
	if ApproximatelyEqual(precision, 0) {
		precision = DefaultPrecision
	}
	precision = 1 / precision
	return math.Round(value*precision) / precision
}

// Clamp return the result of the "value" clamped by
func Clamp(value, lowerLimit, upperLimit float64) float64 {
	if value < lowerLimit {
		return lowerLimit
	} else if value > upperLimit {
		return upperLimit
	}
	return value
}

// ApproximatelyEqual function to test if two real numbers are (almost) equal
func ApproximatelyEqual(a, b float64) bool {
	epsilon := math.SmallestNonzeroFloat64
	difference := a - b
	return difference < epsilon && difference > -epsilon
}

// Max return maximum value in slice
func Max(vs ...float64) float64 {
	if len(vs) == 0 {
		return 0
	}
	if len(vs) == 1 {
		return vs[0]
	}
	max := vs[0]
	for _, v := range vs[1:] {
		if v > max {
			max = v
		}
	}
	return max
}

// Min return minimum value in slice
func Min(vs ...float64) float64 {
	if len(vs) == 0 {
		return 0
	}
	if len(vs) == 1 {
		return vs[0]
	}
	min := vs[0]
	for _, v := range vs[1:] {
		if v < min {
			min = v
		}
	}
	return min
}
