package utils

import (
	"context"
	"sync"
	"time"
)

// RunSyncMultipleWorkers run sync multiple workers
func RunSyncMultipleWorkers(ctx context.Context, goroutines int, fn func(ctx context.Context)) {
	var wg sync.WaitGroup
	wg.Add(goroutines)
	for i := 0; i < goroutines; i++ {
		go func() {
			defer wg.Done()
			fn(ctx)
		}()
	}
	wg.Wait()
}

// RunAsyncMultipleWorkers run async multiple workers and return channel to control them
func RunAsyncMultipleWorkers(ctx context.Context, goroutines int, buffer int, fn func(context.Context, chan<- interface{})) <-chan interface{} {
	ch := make(chan interface{}, buffer)
	var wg sync.WaitGroup
	wg.Add(goroutines)
	for i := 0; i < goroutines; i++ {
		go func() {
			defer wg.Done()
			fn(ctx, ch)
		}()
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	return ch
}

// MergeChannels merge multiple channels into one
func MergeChannels(input ...<-chan interface{}) <-chan interface{} {
	out := make(chan interface{})

	var wg sync.WaitGroup
	wg.Add(len(input))
	for _, ch := range input {
		go func(ch <-chan interface{}) {
			for v := range ch {
				out <- v
			}
			wg.Done()
		}(ch)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

// GetMessageOrTimeout get message or timeout
func GetMessageOrTimeout(timeout time.Duration, msg chan []byte, def []byte) []byte {
	timer := time.NewTimer(timeout)
	select {
	case <-timer.C:
		return def
	case m := <-msg:
		return m
	}
}
