package utils

import (
	"math"

	"bitbucket.org/espinpro/go-engine/datatypes"
)

func UniqueDifferenceFromLeft(result datatypes.DataSetStrings, slice1 []string, slice2 []string) {
	slice1Map := datatypes.NewDataSetStrings(slice1...)
	for _, element := range slice2 {
		if !slice1Map.Contains(element) && !result.Contains(element) {
			result.Add(element)
		}
	}
}

func UniqueSliceIntersection(result datatypes.DataSetStrings, slice1 []string, slice2 []string) {
	slice1Map := datatypes.NewDataSetStrings(slice1...)
	for _, element := range slice2 {
		if slice1Map.Contains(element) && !result.Contains(element) {
			result.Add(element)
		}
	}
}

func UniqSliceDifference(result datatypes.DataSetStrings, slice1 []string, slice2 []string) {
	UniqueDifferenceFromLeft(result, slice1, slice2)
	UniqueDifferenceFromLeft(result, slice2, slice1)
}

const SizeOfChannel = 100

func BatchSlice(count int, slice []interface{}) <-chan []interface{} {
	if count <= 0 {
		count = 1
	}
	ch := make(chan []interface{}, SizeOfChannel)
	go func(ch chan []interface{}) {
		defer close(ch)
		l := len(slice)
		if l <= count {
			ch <- slice
			return
		}

		parts := l / count
		for i := 0; i <= parts; i++ {
			idxStart := i * count
			idxStop := int(math.Min(float64(idxStart+count), float64(l)))
			if idxStart == idxStop {
				continue
			}
			ch <- slice[idxStart:idxStop]
		}
		return
	}(ch)
	return ch
}
