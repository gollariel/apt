package utils

import (
	"encoding/binary"
	"encoding/hex"
	"math/big"
	"math/rand"
	"strings"
	"time"
	"unsafe"

	uuid "github.com/satori/go.uuid"
)

// ByteSliceToString cast given bytes to string, without allocation memory
func ByteSliceToString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b)) //nolint
}

// GetUniqueID return unique id
func GetUniqueID() string {
	return uuid.NewV4().String()
}

// GetShortID return short id
func GetShortID() ([]byte, error) {
	b := make([]byte, 2)   //nolint:gomnd
	_, err := rand.Read(b) //nolint:gosec
	if err != nil {
		return nil, err
	}
	r := make([]byte, 4) //nolint:gomnd
	binary.BigEndian.PutUint32(r, uint32(time.Now().Nanosecond()))
	src := append(b, r...) //nolint:gocritic
	dst := make([]byte, hex.EncodedLen(len(src)))
	hex.Encode(dst, src)
	return dst, nil
}

// GetTinyID return tiny id
func GetTinyID() ([]byte, error) {
	b := make([]byte, 4)   //nolint:gomnd
	_, err := rand.Read(b) //nolint:gosec
	if err != nil {
		return nil, err
	}
	r := make([]byte, 4) //nolint:gomnd
	// time.Now().UnixNano()
	binary.BigEndian.PutUint32(r, uint32(time.Now().Nanosecond()))
	src := append(b, r...) //nolint:gocritic
	val := binary.BigEndian.Uint64(src)
	return []byte(big.NewInt(int64(val)).Text(62))[5:], nil //nolint:gomnd
}

// Between function to get content between two keys
func Between(data string, keys ...string) string {
	var key1, key2 string

	switch {
	case len(keys) == 1: //nolint:gomnd
		key1 = keys[0]
		key2 = keys[0]
	case len(keys) >= 2: //nolint:gomnd
		key1 = keys[0]
		key2 = keys[1]
	default:
		return ""
	}

	if key1 == "" || key2 == "" {
		return ""
	}
	s := strings.Index(data, key1)
	if s <= -1 {
		return ""
	}
	s += len(key1)
	e := strings.Index(data[s:], key2)
	if e <= -1 {
		return ""
	}
	return strings.TrimSpace(data[s : s+e])
}
