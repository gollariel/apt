package utils

import (
	"os"

	"github.com/google/gops/agent"
)

// RunGopsInstance run gops instance and use GOPS_ADDR if it present as listener addr
func RunGopsInstance() error {
	gopsAddr := os.Getenv("GOPS_ADDR")
	if gopsAddr == "" {
		gopsAddr = ":6060"
	}
	if err := agent.Listen(agent.Options{Addr: gopsAddr}); err != nil {
		return err
	}
	return nil
}
