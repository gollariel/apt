package utils

import (
	"fmt"
)

func Recover(fn func() error) (err error) {
	defer func() {
		if rval := recover(); rval != nil {
			switch val := rval.(type) {
			case error:
				err = fmt.Errorf("panic recover: %w", val)
			case string:
				err = fmt.Errorf("panic recover: %s", val)
			default:
				err = fmt.Errorf("panic recover: %v", val)
			}
		}
	}()
	if fn != nil {
		err = fn()
	}
	return err
}
