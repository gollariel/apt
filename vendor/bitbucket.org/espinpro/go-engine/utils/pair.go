package utils

import "math"

// CantorPair make one value from two values
func CantorPair(x, y float64) float64 {
	return ((x+y)*(x+y+1))/2 + y
}

// ReverseCantorPair return two values from one
func ReverseCantorPair(z float64) (float64, float64) {
	w := math.Floor((math.Sqrt(8*z+1) - 1) / 2) //nolint:gomnd
	t := (w*w + w) / 2                          //nolint:gomnd
	y := z - t
	x := w - y
	return x, y
}
