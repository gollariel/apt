package utils

import (
	"time"

	"github.com/pkg/errors"
)

// Retry call method with retries
func Retry(times int, timeout time.Duration, method func(i int) error) (err error) {
	if times <= 0 {
		times = 1
	}

	var totalSleepTime time.Duration
	for i := 0; i < times; i++ {
		err = method(i)
		if err != nil && errors.Cause(err) == errors.Cause(ErrRetryWithContinue) {
			time.Sleep(timeout) // do not spam with retries
			totalSleepTime += timeout
			timeout += time.Duration(i) * timeout
			continue
		}
		break
	}
	return errors.Wrapf(err, "total sleep time: %d", totalSleepTime)
}
