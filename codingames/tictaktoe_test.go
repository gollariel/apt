package codingames

import (
	"fmt"
	"testing"
)

const (
	AllySign     = 1
	OpponentSign = 2
)

func TestTicTakToe(t *testing.T) {
	field := [3][3]int{
		{2, 0, 0},
		{0, 1, 0},
		{0, 2, 1},
	}
	x, y := NextTurn(field)
	field[x][y] = AllySign
	fmt.Println(x, y, field)
}

func NextTurn(field [3][3]int) (int, int) {

	rows := map[int]int{}
	columns := map[int]int{}
	diagonals := map[int]int{}
	ownRows := map[int]int{}
	ownColumns := map[int]int{}
	ownDiagonals := map[int]int{}
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if field[i][j] == OpponentSign {
				rows[i]++
				columns[j]++
			}
			if field[i][j] == AllySign {
				ownRows[i]++
				ownColumns[j]++
			}
		}
	}
	d := 2
	for i := 0; i < 3; i++ {
		if field[i][i] == OpponentSign {
			diagonals[1]++
		}
		if field[d][i] == OpponentSign {
			diagonals[2]++
		}
		if field[i][i] == AllySign {
			ownDiagonals[1]++
		}
		if field[d][i] == AllySign {
			ownDiagonals[2]++
		}
		d--
	}

	if len(rows) == 0 && len(columns) == 0 && len(diagonals) == 0 {
		return 1, 1
	}

	for i, v := range ownRows {
		if v == 2 {
			for j, v := range field[i] {
				if v == 0 {
					return i, j
				}
			}
		}
	}

	for j, v := range ownColumns {
		if v == 2 {
			for i := 0; i < 3; i++ {
				v := field[i][j]
				if v == 0 {
					return i, j
				}
			}
		}
	}

	for d, v := range ownDiagonals {
		if v == 2 {
			if d == 1 {
				for i := 0; i < 3; i++ {
					if field[i][i] == 0 {
						return i, i
					}
				}
			}
			if d == 2 {
				dir := 2
				for i := 0; i < 3; i++ {
					if field[dir][i] == 0 {
						return dir, i
					}
					dir--
				}
			}
		}
	}

	for i, v := range rows {
		if v == 2 {
			for j, v := range field[i] {
				if v == 0 {
					return i, j
				}
			}
		}
	}

	for j, v := range columns {
		if v == 2 {
			for i := 0; i < 3; i++ {
				v := field[i][j]
				if v == 0 {
					return i, j
				}
			}
		}
	}

	for d, v := range diagonals {
		if v == 2 {
			if d == 1 {
				for i := 0; i < 3; i++ {
					if field[i][i] == 0 {
						return i, i
					}
				}
			}
			if d == 2 {
				dir := 2
				for i := 0; i < 3; i++ {
					if field[dir][i] == 0 {
						return dir, i
					}
					dir--
				}
			}
		}
	}

	if (field[0][0] == OpponentSign || field[2][2] == OpponentSign || field[0][2] == OpponentSign || field[2][0] == OpponentSign) && (field[0][1] == OpponentSign || field[1][0] == OpponentSign || field[1][2] == OpponentSign || field[2][1] == OpponentSign) {
		if field[0][1] == 0 {
			return 0, 1
		}
		if field[1][0] == 0 {
			return 1, 0
		}
		if field[1][2] == 0 {
			return 1, 2
		}
		if field[2][1] == 0 {
			return 2, 1
		}
	}
	if field[0][0] == 0 {
		return 0, 0
	}
	if field[2][2] == 0 {
		return 2, 2
	}
	if field[0][2] == 0 {
		return 0, 2
	}
	if field[2][0] == 0 {
		return 2, 0
	}

	for i, rows := range field {
		for j, v := range rows {
			if v == 0 {
				return i, j
			}
		}
	}
	return 0, 0
}
