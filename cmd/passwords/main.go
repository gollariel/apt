package main

import (
	"bufio"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"

	"go.mongodb.org/mongo-driver/mongo/options"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/mongodb"
	"bitbucket.org/espinpro/go-engine/utils"
)

type Object struct {
	Hash     string `bson:"_id"`
	Password string `bson:"password"`
}

var upsert = true

func main() {
	file := os.Getenv("FILE_TO_READ")
	if file == "" {
		fastlog.Fatalw("env FILE_TO_READ not set")
	}

	start := time.Now()
	readFile, err := os.Open(file)
	if err != nil {
		fastlog.Fatalw("Error reading file", "err", err)
	}

	ch := make(chan string, 10000)
	go func() {
		fileScanner := bufio.NewScanner(readFile)
		fileScanner.Split(bufio.ScanLines)
		for fileScanner.Scan() {
			password := fileScanner.Text()
			ch <- password
		}
		close(ch)
	}()

	db, err := mongodb.Default()
	if err != nil {
		fastlog.Fatalw("Error connecting to DB", "err", err)
	}

	opt := options.InsertMany()
	opt.SetOrdered(false)
	utils.RunSyncMultipleWorkers(context.Background(), 20, func(ctx context.Context) {
		var models []mongo.WriteModel
		for password := range ch {
			sum := sha256.Sum256([]byte(password))
			hash := hex.EncodeToString(sum[:])
			models = append(models, &mongo.UpdateOneModel{
				Filter: bson.D{{Key: "_id", Value: hash}},
				Update: bson.D{{Key: "$set", Value: Object{
					Hash:     hash,
					Password: password,
				}}},
				Upsert: &upsert,
			})

			if len(models) == 1000 {
				_, err := db.Collection("weakpass").BulkWrite(ctx, models)
				if err != nil {
					fastlog.Errorw("Error setting data to DB", "err", err)
				}
				models = []mongo.WriteModel{}
			}
		}
	})

	err = readFile.Close()
	if err != nil {
		fastlog.Fatalw("Error closing file", "err", err)
	}
	fastlog.Infow("Total execution time", "duration", time.Since(start))
}
