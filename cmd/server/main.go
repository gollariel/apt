package main

import (
	"bitbucket.org/espinpro/go-engine/server/middlewares"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/server"
	"github.com/google/gops/agent"
	"github.com/kelseyhightower/envconfig"
)

const prefix = "APT"

var version string

// Config contains configs for server
type Config struct {
	GopsAddr     string        `required:"true" split_words:"true"`
	Addr         string        `required:"true"`
	ReadTimeout  time.Duration `required:"true" split_words:"true"`
	WriteTimeout time.Duration `required:"true" split_words:"true"`
	Source       string        `required:"true"`
}

// GetConfigFromEnv return apt configs bases on environment variables
func GetConfigFromEnv() (*Config, error) {
	c := new(Config)
	err := envconfig.Process(prefix, c)
	return c, err
}

func main() {
	fastlog.Infow("Running instance", "version", version)
	c, err := GetConfigFromEnv()
	if err != nil {
		fastlog.Errorw("Error getting config", "err", err)
	}

	router := server.Root()
	router.Add("/:source", server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		v, ok := r.Context().Value(server.CtxValues).(map[string]string)
		if !ok {
			fastlog.Errorw("Error not found params", "v", v)
			w.WriteHeader(http.StatusNotFound)
			return
		}
		s := v["source"]
		if !strings.Contains(s, ".") {
			s = strings.Join([]string{s, ".sh"}, "")
		}
		if strings.Contains(s, "..") {
                        fastlog.Errorw("Error not found params", "v", v)
                        w.WriteHeader(http.StatusNotFound)
                        return
                }

		file := strings.Join([]string{c.Source, "/", s}, "")
		info, err := os.Stat(file)
		if os.IsNotExist(err) || info.IsDir() {
			fastlog.Errorw("Error not found source", "file", file)
			w.WriteHeader(http.StatusNotFound)
			return
		}

		raw, err := ioutil.ReadFile(file)
		if err != nil {
			fastlog.Errorw("Error reading source", "file", file)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		_, err = w.Write(raw)
		if err != nil {
			fastlog.Errorw("Error writing response", "file", file)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})))
	router.Add("/", server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("TEST")
		w.WriteHeader(http.StatusOK)
	})))

	s := &http.Server{
		Addr:         c.Addr,
		Handler:      middlewares.Recover(router.Handler(nil)),
		ReadTimeout:  c.ReadTimeout,
		WriteTimeout: c.WriteTimeout,
	}
	s.RegisterOnShutdown(func() {
		err := s.Close()
		if err != nil {
			fastlog.Errorw("Error close server", "err", err)
		}
	})

	if err := agent.Listen(agent.Options{Addr: c.GopsAddr}); err != nil {
		fastlog.Errorw("Failed to run gops agent", "err", err)
	}

	if err := s.ListenAndServe(); err != nil {
		fastlog.Fatalw("Error listen server", "err", err)
	}
}
