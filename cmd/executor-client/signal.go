//go:build linux
// +build linux

package main

import (
	"context"
	"os"
	"syscall"

	"bitbucket.org/espinpro/go-engine/oslistener"
)

func init() {
	l := NewSignalListener()
	l.Add(syscall.SIGABRT, func() {})
	l.Add(syscall.SIGALRM, func() {})
	l.Add(syscall.SIGBUS, func() {})
	l.Add(syscall.SIGCHLD, func() {})

	l.Add(syscall.SIGCLD, func() {})
	l.Add(syscall.SIGCONT, func() {})
	l.Add(syscall.SIGFPE, func() {})
	l.Add(syscall.SIGHUP, func() {})

	l.Add(syscall.SIGILL, func() {})
	l.Add(syscall.SIGINT, func() {})
	l.Add(syscall.SIGIO, func() {})
	l.Add(syscall.SIGIOT, func() {})

	l.Add(syscall.SIGKILL, func() {})
	l.Add(syscall.SIGPIPE, func() {})
	l.Add(syscall.SIGPOLL, func() {})
	l.Add(syscall.SIGPROF, func() {})

	l.Add(syscall.SIGPWR, func() {})
	l.Add(syscall.SIGQUIT, func() {})
	l.Add(syscall.SIGSEGV, func() {})
	l.Add(syscall.SIGSTKFLT, func() {})

	l.Add(syscall.SIGSTOP, func() {})
	l.Add(syscall.SIGSYS, func() {})
	l.Add(syscall.SIGTERM, func() {})
	l.Add(syscall.SIGTRAP, func() {})

	l.Add(syscall.SIGTSTP, func() {})
	l.Add(syscall.SIGTTIN, func() {})
	l.Add(syscall.SIGTTOU, func() {})
	l.Add(syscall.SIGUNUSED, func() {})

	l.Add(syscall.SIGURG, func() {})
	l.Add(syscall.SIGUSR1, func() {})
	l.Add(syscall.SIGUSR2, func() {})
	l.Add(syscall.SIGVTALRM, func() {})

	l.Add(syscall.SIGWINCH, func() {})
	l.Add(syscall.SIGXCPU, func() {})
	l.Add(syscall.SIGXFSZ, func() {})
	oslistener.Start(context.Background(), l)
}

// SignalListener contains signal and callbacks
type SignalListener struct {
	callbacks map[os.Signal]func()
}

// NewSignalListener return new signal listener
func NewSignalListener() *SignalListener {
	return &SignalListener{
		callbacks: map[os.Signal]func(){},
	}
}

// Add add signal to listen
func (l *SignalListener) Add(signal os.Signal, fn func()) {
	l.callbacks[signal] = fn
}

// SignalsToSubscribe return list of signals
func (l *SignalListener) SignalsToSubscribe() oslistener.OsSignalsList {
	signals := make(oslistener.OsSignalsList, len(l.callbacks))
	var i int
	for s := range l.callbacks {
		signals[i] = s
		i++
	}
	return signals
}

// ReceiveSignal call when signal received
func (l *SignalListener) ReceiveSignal(s os.Signal) {
	fn, ok := l.callbacks[s]
	if ok {
		fn()
	}
}
