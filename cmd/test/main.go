package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	file, err := os.Open("/home/gollariel/scripts/targets_full.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var hosts []string
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		fmt.Println(scanner.Text())
		hosts = append(hosts, scanner.Text())
	}
	err = os.WriteFile("/home/gollariel/scripts/targets_full_semicolon.txt", []byte(strings.Join(hosts, "; ")), 0666)
	if err != nil {
		log.Fatal(err)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
