package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"bitbucket.org/espinpro/go-engine/fastlog"
)

type HookPostPublishedPayload struct {
	PostID      string
	PostTitle   string
	ChannelID   string
	ChannelName string
}

type Payload struct {
	Channel   string `json:"channel"`
	Username  string `json:"username"`
	Text      string `json:"text"`
	IconEmoji string `json:"icon_emoji"`
}

func main() {
	http.ListenAndServe(":6061", http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		p := HookPostPublishedPayload{}
		b, err := io.ReadAll(request.Body)
		if err != nil {
			fastlog.Errorw("Error reading event", "err", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		fmt.Println(string(b))
		err = json.Unmarshal(b, &p)
		if err != nil {
			fastlog.Errorw("Error unmarshaling event", "err", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		// Important new policy Post Title <link to post preview URL> published now in Channel Name <link to Channel URL>
		data, err := json.Marshal(Payload{
			Channel:   "#internal-policy",
			Username:  "Smarp Notifier",
			Text:      fmt.Sprintf("Important new policy <https://smarp.smarpshare.com/#/preview/%s|%s> published now in <https://smarp.smarpshare.com/#/?channelId=%s|%s>", p.PostID, p.PostTitle, p.ChannelID, p.ChannelName),
			IconEmoji: ":smarp:",
		})
		if err != nil {
			fastlog.Errorw("Error marshaling payload", "err", err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		fastlog.Infow("Send new slack message", "data", string(data))
		resp, err := http.Post("https://hooks.slack.com/services/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", "application/json", bytes.NewBuffer(data))
		if err != nil || resp.StatusCode != http.StatusOK {
			fastlog.Errorw("Error marshaling payload", "err", err, "code", resp.StatusCode)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		writer.WriteHeader(http.StatusOK)
	}))
}
