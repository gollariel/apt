package main

import (
	"fmt"
	"testing"
)

func TestPickSomeone(t *testing.T) {
	result := map[string]int{}
	var prev string
	var sameUser int
	for i := 0; i < 10000; i++ {
		user := pickSomeone([]string{"qwe", "asd", "zxc", "wer", "sdf", "xcv", "ert", "dfg"})
		if _, ok := result[user]; !ok {
			result[user] = 1
		} else {
			result[user]++
		}
		if prev == user {
			sameUser++
		}
		prev = user
	}
	fmt.Println(result, sameUser)
}
