package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"

	"bitbucket.org/espinpro/go-engine/datatypes"
	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/utils/rand"
	"github.com/slack-go/slack"
)

const (
	accessToken          = `xoxb-572212369141-2152288465445-LVKPsfghbEV5E3HtPLTaNuox`
	cmdName       string = "/smartpicker"
	signingSecret string = `d624c3515982e22eef52709dd74105a7`
)

var counter Counter

type Counter struct {
	data map[string]uint64
	mu   sync.RWMutex
}

func (c *Counter) Increment(name string) {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.data == nil {
		c.data = map[string]uint64{}
	}
	if _, ok := c.data[name]; !ok {
		c.data[name] = 0
	}
	c.data[name]++
}

func (c *Counter) GetUserCount(name string) uint64 {
	c.mu.RLock()
	defer c.mu.RUnlock()

	return c.data[name]
}

func (c *Counter) GetUsers(users []string) (uint64, map[string]uint64) {
	c.mu.Lock()
	defer c.mu.Unlock()

	var maxValue uint64
	if c.data == nil {
		c.data = map[string]uint64{}
	}
	for _, name := range users {
		if _, ok := c.data[name]; !ok {
			c.data[name] = 0
		}
	}
	set := datatypes.NewDataSetStrings(users...)
	data := map[string]uint64{}
	for name, count := range c.data {
		if set.Contains(name) {
			data[name] = count
		}
		if count > maxValue {
			maxValue = count
		}
	}
	return maxValue, data
}

func (c *Counter) GetWeights(users []string) map[interface{}]uint64 {
	userPickProbabilities := map[interface{}]uint64{}
	maxValue, data := c.GetUsers(users)
	for name, count := range data {
		fmt.Println(maxValue, count)
		userPickProbabilities[name] = maxValue - count
	}
	return userPickProbabilities
}

func pickSomeone(users []string) string {
	weights := counter.GetWeights(users)
	value := rand.WeightIndex(weights).(string)
	counter.Increment(value)
	fastlog.Infow("Picked value", "weights", fmt.Sprintf("%v", weights), "value", value)

	return value
}

func main() {
	err := http.ListenAndServe(":6061", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cmd, err := slack.SlashCommandParse(r)
		if err != nil {
			fastlog.Errorw("Error parse command", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		//verifier, err := slack.NewSecretsVerifier(r.Header, signingSecret)
		//if err != nil {
		//	fastlog.Errorw("Error getting verifier", "err", err)
		//	w.WriteHeader(http.StatusInternalServerError)
		//	return
		//}
		//
		//if err = verifier.Ensure(); err != nil {
		//	fastlog.Errorw("Error unauthorized", "err", err)
		//	w.WriteHeader(http.StatusUnauthorized)
		//	return
		//}

		if cmd.Command != cmdName {
			fastlog.Errorw("Error wrong command", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		api := slack.New(accessToken)

		var users []string
		var next string
		for {
			var tempUsers []string
			var err error
			tempUsers, next, err = api.GetUsersInConversation(&slack.GetUsersInConversationParameters{
				ChannelID: cmd.ChannelID,
				Cursor:    next,
			})
			if err != nil {
				fastlog.Errorw("Error getting users", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			users = append(users, tempUsers...)
			if next == "" {
				break
			}
		}

		if len(users) == 0 {
			fastlog.Errorw("No any users in channel", "err", err, "users", users)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		user := pickSomeone(users)
		fastlog.Infow("Picked user from participants", "user", user, "users", users)

		params := &slack.Msg{Text: strings.Join([]string{`Hey we picked up: <@`, user, `|user>`}, "")}
		b, err := json.Marshal(params)
		if err != nil {
			fastlog.Errorw("Error marshaling response", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(b)
		if err != nil {
			fastlog.Errorw("Error sending response", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}))
	if err != nil {
		fastlog.Fatalw("Error listen picker", "err", err)
		return
	}
}
