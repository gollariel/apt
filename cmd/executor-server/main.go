package main

import (
	"flag"
	"io"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/server"
)

func main() {
	source := flag.String("source", ".", "Choose dir of commands and result")
	flag.Parse()

	router := server.Root()
	router.Add("/",
		server.NewRoute(http.MethodPut, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			clientID := r.Header.Get("X-Client-ID")
			if clientID == "" {
				return
			}
			clientOS := r.Header.Get("X-Client-OS")
			err := os.MkdirAll(strings.Join([]string{*source, "/", clientID}, ""), 0666)
			if err != nil {
				fastlog.Errorw("Error creating folder", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			defer func() {
				err := r.Body.Close()
				if err != nil {
					fastlog.Errorw("Error closing body", "err", err)
				}
			}()
			data, err := io.ReadAll(r.Body)
			if err != nil {
				fastlog.Errorw("Error getting request", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			if len(data) == 0 {
				w.WriteHeader(http.StatusAccepted)
				return
			}

			file, err := os.OpenFile(strings.Join([]string{*source, "/", clientID, "/", clientOS, "-result.txt"}, ""), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
			if err != nil {
				fastlog.Errorw("Error writing result", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			defer func() {
				err := file.Close()
				if err != nil {
					fastlog.Errorw("Error closing file", "err", err)
				}
			}()

			_, err = file.Write(data)
			if err != nil {
				fastlog.Errorw("Error writing file", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusAccepted)
		})),
		server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			clientID := r.Header.Get("X-Client-ID")
			if clientID == "" {
				return
			}
			clientOS := r.Header.Get("X-Client-OS")
			clientHost := r.Header.Get("X-Client-Host")

			err := os.MkdirAll(strings.Join([]string{*source, "/", clientID}, ""), 0666)
			if err != nil {
				fastlog.Errorw("Error creating folder", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			err = os.WriteFile(strings.Join([]string{*source, "/", clientID, "/", "details.txt"}, ""), []byte(strings.Join([]string{clientID, clientOS, clientHost}, ":")), 0666)
			if err != nil {
				fastlog.Infow("Error writing result", "err", err)
				return
			}

			commands, err := os.ReadFile(strings.Join([]string{*source, "/", clientID, "/", clientOS, "-execute.sh"}, ""))
			if err != nil {
				fastlog.Warnw("Error get execute sh file", "err", err)
			}
			defer func() {
				err = os.WriteFile(strings.Join([]string{*source, "/", clientID, "/", clientOS, "-execute.sh"}, ""), nil, 0666)
				if err != nil {
					fastlog.Infow("Error writing result", "err", err)
					return
				}
			}()

			if len(commands) == 0 {
				w.WriteHeader(http.StatusOK)
				switch clientOS {
				case "linux":
					_, err = w.Write([]byte("uname -a\nifconfig\nls\nlshw\nlscpu\nlsblk\nlsusb\nlspci\nlsscsi"))
					if err != nil {
						fastlog.Errorw("Error writing response", "err", err)
						w.WriteHeader(http.StatusInternalServerError)
						return
					}
				case "windows":
					_, err = w.Write([]byte("systeminfo"))
					if err != nil {
						fastlog.Errorw("Error writing response", "err", err)
						w.WriteHeader(http.StatusInternalServerError)
						return
					}
				}
				return
			}

			w.WriteHeader(http.StatusOK)
			_, err = w.Write(commands)
			if err != nil {
				fastlog.Errorw("Error writing response", "err", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		})),
	)
	err := http.ListenAndServe(":5050", router.Handler(nil))
	if err != nil {
		fastlog.Fatalw("Error listen http server", "err", err)
	}
}
