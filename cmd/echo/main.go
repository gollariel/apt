package main

import (
	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/server"
	"bitbucket.org/espinpro/go-engine/server/middlewares"
	"encoding/json"
	"github.com/google/gops/agent"
	"github.com/kelseyhightower/envconfig"
	"io"
	"net/http"
	"sync"
	"time"
)

const prefix = "ECHO"

var version string

// Config contains configs for server
type Config struct {
	GopsAddr     string        `split_words:"true" default:":6060"`
	Addr         string        `default:"127.0.0.1:8090"`
	ReadTimeout  time.Duration `split_words:"true" default:"10s"`
	WriteTimeout time.Duration `split_words:"true" default:"10s"`
}

// GetConfigFromEnv return apt configs bases on environment variables
func GetConfigFromEnv() (*Config, error) {
	c := new(Config)
	err := envconfig.Process(prefix, c)
	return c, err
}

type LogRequest struct {
	When    string
	Method  string
	Payload interface{}
	Header  http.Header
}

var (
	requests []LogRequest
	mu       sync.RWMutex
)

func main() {
	fastlog.Infow("Running instance", "version", version)
	c, err := GetConfigFromEnv()
	if err != nil {
		fastlog.Errorw("Error getting config", "err", err)
	}

	router := server.Root()
	router.Add("/", server.NewRoute("", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = r.Body.Close()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		mu.Lock()
		if len(requests) == 10 {
			requests = requests[1:]
		}

		var result interface{}
		if json.Valid(data) {
			err := json.Unmarshal(data, &result)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		} else {
			result = data
		}
		requests = append(requests, LogRequest{
			When:    time.Now().Format(time.RFC3339),
			Method:  r.Method,
			Payload: result,
			Header:  r.Header,
		})
		mu.Unlock()
		w.WriteHeader(http.StatusOK)
	})))
	router.Add("/requests", server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mu.RLock()
		data := make([]LogRequest, len(requests))
		copy(data, requests)
		mu.RUnlock()

		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		res, err := json.Marshal(data)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(res)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})))

	s := &http.Server{
		Addr:         c.Addr,
		Handler:      middlewares.Recover(router.Handler(nil)),
		ReadTimeout:  c.ReadTimeout,
		WriteTimeout: c.WriteTimeout,
	}
	s.RegisterOnShutdown(func() {
		err := s.Close()
		if err != nil {
			fastlog.Errorw("Error close server", "err", err)
		}
	})

	if err := agent.Listen(agent.Options{Addr: c.GopsAddr}); err != nil {
		fastlog.Errorw("Failed to run gops agent", "err", err)
	}

	if err := s.ListenAndServe(); err != nil {
		fastlog.Fatalw("Error listen server", "err", err)
	}
}
