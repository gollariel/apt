package main

import (
	"fmt"
	"testing"

	"golang.org/x/crypto/bcrypt"
)

func TestHook(t *testing.T) {
	hash, err := bcrypt.GenerateFromPassword([]byte("test"), bcrypt.DefaultCost)
	fmt.Println(string(hash), err)
}
