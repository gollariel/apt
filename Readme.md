# executor-client
Run client, try to create service for this file, and start sending requests to desired host every hour. Server can return list of commands to execute.
Support additional command: `set-delay 1s`, to start getting commands more frequently

# executor-server
Server which run, and wait for incoming connections, and send commands to execute on machine